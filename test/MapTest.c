
#include "Test.h"
#include "hashmap.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RUN_TEST_N_TIMES 5

static int simpleMapTest(){
	int nrErr = 0,ret;

	hashmap* map = mk_hmap(str_hash_fn, str_eq_fn);
	ASSERT_NEQ((int)map->map, (int)NULL);
	ASSERT_EQ(map->capacity, HMAP_PRESET_SIZE);
//	ASSERT_EQ((int)list->del_fn, (int)free);
	ASSERT_EQ(map->size, 0);

	TEST_INFO("Simple Map Test");
	return nrErr;
}

static int enlargeMapTest(){
	int nrErr = 0,ret, loop=0;

	while ( loop++ < RUN_TEST_N_TIMES ){
		#ifdef HMAP_DESTRUCTORS
			hashmap* map2 = mk_hmap(str_hash_fn, str_eq_fn, str_del_fn);
		#else
			hashmap* map2 = mk_hmap(str_hash_fn, str_eq_fn);
		#endif

		int i, map_size = 100;
		for (i=0; i<map_size; i++){
			char* key2 = utils_cstr_int_to_cstr(i);
			char* v2 = utils_cstr_concat("value_", key2);
			ASSERT_NEQ( hmap_add(map2, key2, v2), 0);
		}

		ASSERT_EQ(map2->size, map_size);

		for (i=0; i<map_size; i++){
			char* key2 = utils_cstr_int_to_cstr(i);
			char* from_map = (char*) hmap_get(map2, key2);
			if (from_map == NULL){
				printf("Failed: (line: %d) - hmap_get(map2, key2) return NULL key=%s\n", __LINE__, key2);
				++nrErr;
			} else {
				ASSERT_EQ(strcmp(from_map, utils_cstr_concat("value_", key2)), 0);
			}

		}
		free_hmap(map2);
	}

	TEST_INFO("Enlarge Map Test");
	return nrErr;
}

void MapTest() {
	PRINT_STARTS
	int nr = 0;

	nr += simpleMapTest();
	nr += enlargeMapTest();

	PRINT_STARTS
	printf("*          MAP TEST:  %s, number of errors %d                *\n",
			nr == 0 ? "Passed" : "Not passed", nr);
	PRINT_STARTS
	printf("\n");
	fflush(stdout);
}

