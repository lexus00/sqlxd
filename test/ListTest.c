
#include "Test.h"
#include "list.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RUN_TEST_N_TIMES 5

static int simpleListTest(){
	int nrErr = 0,ret;

	ArrayList* list = list_mk(free, CSTR_EQ_FN);
	ASSERT_NEQ((int)list->arr, (int)NULL);
	ASSERT_EQ(list->capacity, LIST_INITIAL_SIZE);
	ASSERT_EQ((int)list->del_fn, (int)free);
	ASSERT_EQ((int)list->eq_fn, (int)CSTR_EQ_FN);
	ASSERT_EQ(list->size, 0);

	list_free(list);

	TEST_INFO("Simple Test");
	return nrErr;
}

static int enlargeListTest(){
	int nrErr = 0,ret, loop=0;

	while ( loop++ < RUN_TEST_N_TIMES ){
		ArrayList* list = list_mk(free, CSTR_EQ_FN);
		int i, list_size = 100;
		for (i=0; i<list_size; i++)
			list_add(list, utils_cstr_int_to_cstr(i));

		ASSERT_EQ(list->size, list_size);

		for (i=0; i<list_size; i++){
			int str_cmp = strcmp((char*)list_get(list, i), utils_cstr_int_to_cstr(i));
			ASSERT_EQ(str_cmp, 0);
		}
		list_free(list);
	}

	TEST_INFO("Enlarge List Test");
	return nrErr;
}

static int containsListTest(){
	int nrErr = 0,ret;
	ArrayList* list = list_mk(free, CSTR_EQ_FN);
	int i, list_size = 10;
	for (i=0; i<list_size; i++)
		list_add(list, utils_cstr_int_to_cstr(i));

	ASSERT_EQ(list_index_of(list, utils_cstr_int_to_cstr(3)) >= 0, 1);
	ASSERT_EQ(list_index_of(list, utils_cstr_int_to_cstr(31)) < 0, 1);

	TEST_INFO("Contains List Test");
	return nrErr;
}

void ListTest() {
	PRINT_STARTS
	int nr = 0;

	nr += simpleListTest();
	nr += containsListTest();
	nr += enlargeListTest();

	PRINT_STARTS
	printf("*          LIST TEST:  %s, number of errors %d               *\n",
			nr == 0 ? "Passed" : "Not passed", nr);
	PRINT_STARTS
	printf("\n");
	fflush(stdout);
}

