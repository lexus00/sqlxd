#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <list.h>
#include <utils.h>
#include <sqlite3.h>
#include <macros.h>

#include "Test.h"

#define PEOPLE_XML "test/data/people.xml"
#define PEOPLE_JSON "test/data/people.json"
#define PEOPLE_S_XML "test/data/people_simple.xml"
#define PEOPLE_S_JSON "test/data/people_simple.json"


typedef struct ExpData {
	int nRows, nCols, curRow, numOfErr;
	char ***table; // char[0][*] <- header
} ExpData;

ExpData* initExpData(int nRows, int nCols){
	ExpData* data = (ExpData*) malloc (sizeof(ExpData));
	data->nRows = nRows;
	data->nCols = nCols;
	data->curRow = 0;
	data->numOfErr = 0;
	return data;
}

sqlite3* initDB(){
	sqlite3* db;
	if (sqlite3_open(0, &db) != SQLITE_OK){
		printf("Cannot open database: \n");
		return 0;
	}

	char* sql = (char*)__sqlxd_malloc(512);
	sprintf(sql, "CREATE TABLE Salary (id integer PRIMARY KEY, person_id integer, annual REAL);");
	__sqlxd_execute(db, sql, 0,0, 0);
	sprintf(sql, "INSERT INTO Salary  values (1, 34, 1034);");
	__sqlxd_execute(db, sql, 0,0, 0);
	sprintf(sql, "INSERT INTO Salary  values (2, 132, 20132);");
	__sqlxd_execute(db, sql, 0,0, 0);
	__sqlxd_free(sql);

	return db;
}
sqlite3* initDBAndLoadXML(char* path){
	sqlite3* db = initDB();
	sqlxd_load_xml(db, path);
	return db;
}
sqlite3* initDBAndLoadJSON(char* path){
	sqlite3* db = initDB();
	sqlxd_load_json(db, path);
	return db;
}


#define TEST_LIST_SIZE_AND_REMOVE(list, expectedSize) \
	ASSERT_EQ(list->size, expectedSize) \
	list_free(list);


void comp_cstr(ExpData* data, char* str1, char* str2, char* err_pref){
	if ( !utils_cstr_eq_fn(str1, str2)){
		printf("\t%s: %s <-> %s\n", err_pref, str1, str2);
		++data->numOfErr;
	}
}
void verify_result(sqlite3* db, sqlite3_stmt *stmt, void* ptr){
	ExpData* data = (ExpData*)ptr;
	int idx, nCols = sqlite3_column_count(stmt);
	++data->curRow;

	if (nCols != data->nCols || data->curRow > data->nRows){
		if (nCols != data->nCols)
			printf("\tIncorrect table dimension num of cols are "
					"diffrend exp: %d presend %d\n", data->nCols, nCols);
		++data->numOfErr;
		return;
	}

	if (data->curRow == 1){
		for(idx=0; idx<nCols; ++idx){
			char* value = (char*)sqlite3_column_name(stmt, idx);
			comp_cstr(data, data->table[0][idx], value, "Diffrend column name");
		}
	}

	for(idx=0; idx<nCols; ++idx){
		char* value = (char*) sqlite3_column_text(stmt, idx);
		comp_cstr(data, data->table[data->curRow][idx], value, "Diffrend row value");
	}
}
static int _verifyQueryAndCloseDB(sqlite3* db, char* sql, ExpData* data){
	__sqlxd_execute(db, sql, verify_result, data, 0);
	if (data->curRow != data->nRows){	\
		printf("\t%s\n\tIncorrect table dimension: num ofrows are diffrent, "
				"exp: %d, present: %d\n", sql, data->nRows, data->curRow);
		++data->numOfErr;
	}
	int nErr = data->numOfErr;
	sqlite3_close(db);
	return nErr;
}
static int systemTestQuery(char* testname, char* sql, ExpData* data,
		char* xmlPath, char* jsonPath){
	sqlite3* db = initDBAndLoadXML( xmlPath );
	int nrErr = _verifyQueryAndCloseDB(db, sql, data);

	if (jsonPath != 0){
		data->curRow = 0;
		data->numOfErr = 0;
		db = initDBAndLoadJSON( jsonPath );
		nrErr += _verifyQueryAndCloseDB(db, sql, data);
	}

	free(data);
	TEST_INFO(testname)
	fflush(stdout);
	return nrErr;
}


static int systemTestErrorQuery( char* sql, int errorCode){
	int nrErr=0, ret;

	sqlite3* db = initDBAndLoadXML( PEOPLE_XML );
	ASSERT_EQ(__sqlxd_execute(db, sql, 0, 0, 1), errorCode);
	sqlite3_close(db);

	db = initDBAndLoadJSON( PEOPLE_JSON );
	ASSERT_EQ(__sqlxd_execute(db, sql, 0, 0, 1), errorCode);
	sqlite3_close(db);

	return nrErr;
}


///	----------------  TESTY  --------------
static int testLoadData(){
	int nrErr=0, ret;
	sqlite3* db = initDB();
	ArrayList* list;

	TEST_LIST_SIZE_AND_REMOVE(sqlxd_get_loaded_docs(db), 0)
	sqlxd_load_xml(db, "test/data/people.xml");
	TEST_LIST_SIZE_AND_REMOVE(sqlxd_get_loaded_docs(db), 1)
	sqlxd_load_xml(db, "test/data/people_simple.xml");
	TEST_LIST_SIZE_AND_REMOVE(sqlxd_get_loaded_docs(db), 1)
	sqlxd_load_xml(db, "test/data/payTable.xml");

	list=sqlxd_get_loaded_docs(db);
	ASSERT_EQ(list->size, 2)

	ASSERT_EQ(list_contains(list, "people"), 1)
	ASSERT_EQ(list_contains(list, "paytable"), 1)
	ASSERT_EQ(list_contains(list, "cars"), 0)
	list_free(list);

	sqlite3_close(db);
	TEST_INFO("LoadData")
	fflush(stdout);
	return nrErr;
}


static int testSimpleQuery1(){
	ExpData* data = initExpData(1, 9);
	data->table = (char***) malloc (sizeof (char**) * 2);
	data->table[0] = (char*[9]) {"person.#id", "person.#id.#", "person.firstname",
		"person.firstname.#", "person.age", "person.age.#", "person.hobbies",
		"person.hobbies.hobby", "person.hobbies.hobby.#"};
	data->table[1] = (char*[9]) {"34", "34", "John", "John", "32", "32", "", "Dogs", "Dogs"};

	char* sql = "selectxml person.* from people.person as person";
	return systemTestQuery("SimpleQuery 1", sql, data, PEOPLE_S_XML, PEOPLE_S_JSON);
}

static int testSimpleQuery2(){
	ExpData* data = initExpData(3, 2);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[2]) {"hobby", "hobby.#"};
	data->table[1] = (char*[2]) {"Dogs", "Dogs"};
	data->table[2] = (char*[2]) {"Cats", "Cats"};
	data->table[3] = (char*[2]) {"Parrots", "Parrots"};

	char* sql = "selectxml hobby.* from people.person.hobbies.hobby as hobby";
	return systemTestQuery("SimpleQuery 2", sql, data, PEOPLE_S_XML, PEOPLE_S_JSON);
}

static int testSimpleQuery3(){
	ExpData* data = initExpData(3, 2);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[2]) {"hobby", "hobby.#"};
	data->table[1] = (char*[2]) {"Dogs", "Dogs"};
	data->table[2] = (char*[2]) {"Cats", "Cats"};
	data->table[3] = (char*[2]) {"Parrots", "Parrots"};

	char* sql = "selectxml hobby.* from people.?.?.hobby as hobby";
	return systemTestQuery("SimpleQuery 3", sql, data, PEOPLE_S_XML, PEOPLE_S_JSON);
}

static int testSimpleQuery4(){
	ExpData* data = initExpData(3, 2);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[2]) {"hobby", "hobby.#"};
	data->table[1] = (char*[2]) {"Dogs", "Dogs"};
	data->table[2] = (char*[2]) {"Cats", "Cats"};
	data->table[3] = (char*[2]) {"Parrots", "Parrots"};

	char* sql = "selectxml hobby.* from people.*.hobby as hobby";
	return systemTestQuery("SimpleQuery 4", sql, data, PEOPLE_S_XML, PEOPLE_S_JSON);
}

static int testSimpleQuery5(){
	ExpData* data = initExpData(1, 3);
	data->table = (char***) malloc (sizeof (char**) * 2);
	data->table[0] = (char*[3]) {"person.#id.#", "person.firstname.#", "person.age.#"};
	data->table[1] = (char*[3]) {"34", "John", "32"};

	char* sql = "selectxml person.?.# from people.person as person";
	return systemTestQuery("SimpleQuery 5", sql, data, PEOPLE_S_XML, PEOPLE_S_JSON);
}

static int testSimpleQuery6(){
	ExpData* data = initExpData(1, 4);
	data->table = (char***) malloc (sizeof (char**) * 2);
	data->table[0] = (char*[4]) {"person.#id.#", "person.firstname.#",
		"person.age.#", "person.hobbies.hobby.#"};
	data->table[1] = (char*[4]) {"34", "John", "32", "Dogs"};

	char* sql = "selectxml person.*.# from people.person as person";

	return systemTestQuery("SimpleQuery 6", sql, data, PEOPLE_S_XML, PEOPLE_S_JSON);
}


static int testNaturalJoinQuery(){
	ExpData* data = initExpData(3, 3);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[3]) {"person.#id.#", "person.firstname.#", "hobby.#"};
	data->table[1] = (char*[3]) {"34", "John", "Cats"};
	data->table[2] = (char*[3]) {"34", "John", "Dogs"};
	data->table[3] = (char*[3]) {"34", "John", "Parrots"};

	char* sql = "SELECTXML person.#id.#, person.firstname.#, hobby.# "
		"FROM people.person as person NATURAL JOIN person.hobbies.hobby as hobby";

	return systemTestQuery("NaturalJoin Query", sql, data, PEOPLE_S_XML, PEOPLE_S_JSON);
}

static int testQueryWithCast(){
	ExpData* data = initExpData(3, 3);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[3]) {"person.#id", "id", "person.firstname"};
	data->table[1] = (char*[3]) {"34", "34", "John"};
	data->table[2] = (char*[3]) {"31", "31", 0};
	data->table[3] = (char*[3]) {"132", "132", "Jan"};

	char* sql = "selectxml person.#id, CAST(person.#id as INTEGER) as id,"
    		"person.firstname from people.person as person;";

	return systemTestQuery("Query with Cast", sql, data, PEOPLE_XML, PEOPLE_JSON);
}

static int testQueryOrderByStrCol(){
	ExpData* data = initExpData(3, 3);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[3]) {"person.#id", "id", "person.firstname"};
	data->table[1] = (char*[3]) {"132", "132", "Jan"};
	data->table[2] = (char*[3]) {"31", "31", 0};
	data->table[3] = (char*[3]) {"34", "34", "John"};

	char* sql = "SELECTXML person.#id, CAST(person.#id as INTEGER) as id,"
    		"person.firstname FROM people.person as person ORDER BY person.#id;";

	return systemTestQuery("Query Ordered By Str Col", sql, data, PEOPLE_XML, PEOPLE_JSON);
}

static int testQueryOrderByCastedStrCol(){
	ExpData* data = initExpData(3, 3);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[3]) {"person.#id", "id", "person.firstname"};
	data->table[1] = (char*[3]) {"31", "31", 0};
	data->table[2] = (char*[3]) {"34", "34", "John"};
	data->table[3] = (char*[3]) {"132", "132", "Jan"};

	char* sql = "SELECTXML person.#id, CAST(person.#id as INTEGER) as id,"
    		"person.firstname FROM people.person as person "
    		"ORDER BY CAST(person.#id as INTEGER);";

	return systemTestQuery("Query Ordered By Casted Str Col",
			sql, data, PEOPLE_XML, PEOPLE_JSON);
}

static int testQueryWhere(){
	ExpData* data = initExpData(2, 3);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[3]) {"person.#id", "person.firstname", "person.lastname"};
	data->table[1] = (char*[3]) {"34", "John", "Foo"};
	data->table[2] = (char*[3]) {"132", "Jan", "Nowak"};

	char* sql = "SELECTXML person.#id,  person.firstname, person.lastname "
		"FROM people.person as person WHERE CAST(person.#id as INTEGER) > 32;";

	return systemTestQuery("Query Where", sql, data, PEOPLE_XML, PEOPLE_JSON);
}

static int testQueryGroupBy(){
	ExpData* data = initExpData(3, 3);
	data->table = (char***) malloc (sizeof (char**) * 4);
	data->table[0] = (char*[3]) {"person.#id", "person.firstname", "count(*)"};
	data->table[1] = (char*[3]) {"132", "Jan", "1"};
	data->table[2] = (char*[3]) {"31", 0, "1"};
	data->table[3] = (char*[3]) {"34", "John", "1"};

	char* sql = "SELECTXML person.#id, person.firstname, count(*) "
			"FROM people.person as person GROUP BY person.#id";

	return systemTestQuery("Query with Group By", sql, data, PEOPLE_XML, PEOPLE_JSON);
}

static int testQueryGroupByHaving(){
	ExpData* data = initExpData(2, 3);
	data->table = (char***) malloc (sizeof (char**) * 3);
	data->table[0] = (char*[3]) {"person.#id", "person.firstname", "count(*)"};
	data->table[1] = (char*[3]) {"132", "Jan", "1"};
	data->table[2] = (char*[3]) {"34", "John", "1"};

	char* sql = "SELECTXML person.#id, person.firstname, count(*) "
			"FROM people.person as person "
			"GROUP BY person.#id HAVING person.#id <> '31'";

	return systemTestQuery("Query with Group By and Having",
			sql, data, PEOPLE_XML, PEOPLE_JSON);
}

static int testQueryAggregation(){
	ExpData* data = initExpData(1, 1);
	data->table = (char***) malloc (sizeof (char**) * 2);
	data->table[0] = (char*[1]) {"value"};
	data->table[1] = (char*[1]) {"66.0"};

	char* sql = "SELECTXML ROUND( AVG( CAST(person.#id as INTEGER))) as 'value'"
			"FROM people.person as person";

	return systemTestQuery("Query Aggregation", sql, data, PEOPLE_XML, PEOPLE_JSON);
}

static int testQueryJoinWithSqlTable(){
	ExpData* data = initExpData(2, 3);
	data->table = (char***) malloc (sizeof (char**) * 3);
	data->table[0] = (char*[3]) {"person.#id", "person.firstname", "annual"};
	data->table[1] = (char*[3]) {"34", "John", "1034.0"};
	data->table[2] = (char*[3]) {"132", "Jan", "20132.0"};

	char* sql = "SELECTXML person.#id, person.firstname, salary.annual "
			"FROM people.person as person "
            "INNER JOIN salary as salary on salary.person_id = CAST(person.#id as INTEGER)";

	return systemTestQuery("Query join with SQL Table", sql, data, PEOPLE_XML, PEOPLE_JSON);
}


static int testQueryErrorDetection(){
	char* sql = "selectxml CAST(person.* AS INTEGER) from people.person as person";
	int nrErr = systemTestErrorQuery(sql, SQLxD_PATTERN_NOT_IMPLICIT);

	sql = "selectxml person.#id from people.person";
	nrErr += systemTestErrorQuery(sql, SQLxD_ALIAS_NULL);

	sql = "selectxml person.[?nodeid?], person.#id.# from people.person as person";
	nrErr += systemTestErrorQuery(sql, SQLxD_PATTERN_FAULTY);

	sql = "selectxml person.* from people.person as person "
			"NATURAL JOIN person.hobbies as person";
	nrErr += systemTestErrorQuery(sql, SQLxD_ALIAS_NOT_UNIQUE);

	sql = "selectxml person.ala$ma&kota from people.person as person";
	nrErr += systemTestErrorQuery(sql, SQLxD_PATTERN_FAULTY);

	TEST_INFO("Query Error Detection")
	fflush(stdout);
	return nrErr;
}


// <!--------- 				MAIN 		-------------!>
void SystemTest(){
	PRINT_STARTS
    int nr = 0;

    nr += testLoadData();
    nr += testSimpleQuery1();
    nr += testSimpleQuery2();
    nr += testSimpleQuery3();
    nr += testSimpleQuery4();
    nr += testSimpleQuery5();
    nr += testSimpleQuery6();

    nr += testNaturalJoinQuery();
    nr += testQueryWithCast();
	nr += testQueryOrderByStrCol();
	nr += testQueryOrderByCastedStrCol();
	nr += testQueryWhere();
	nr += testQueryGroupBy();
	nr += testQueryGroupByHaving();
	nr += testQueryAggregation();
	nr += testQueryJoinWithSqlTable();
	nr += testQueryErrorDetection();

    PRINT_STARTS
	printf("*          SYSTEM TEST: %s, number of errors %d              *\n",
			nr == 0 ? "Passed" : "Not passed", nr);
	PRINT_STARTS
	printf("\n");
	fflush(stdout);
}
