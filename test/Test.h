#ifndef __TESTS__H__
#define __TESTS__H__

#define ASSERT_EQ(expr,expectedValue) \
	ret = (expr);   \
	if(ret != (expectedValue)){  \
		printf("Failed: (line: %d) - " #expr " ret: %d, exp: %d\n",__LINE__, ret, expectedValue);  \
		++nrErr; \
	}

#define ASSERT_NEQ(expr,expectedValue) \
	ret = (expr);   \
	if(ret == (expectedValue)){  \
		printf("Failed: (line: %d) - " #expr " ret: %d, exp: %d\n",__LINE__, ret, expectedValue);  \
		++nrErr; \
	}

#define TEST_INFO(str) \
	if (nrErr){ \
		printf("Test '%s' not passed number of error %d\n\n", str, nrErr);  \
	} else {  \
		printf("*          Test '%s' passed   %*s\n",  str, 37 - strlen(str), "*" );  \
	}


#define PRINT_STARTS printf("*****************************************************************\n");



void StructTest();
void ListTest();
void MapTest();
void SystemTest();


#endif // __TESTS__H__

