#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sqlite3.h"
#include "sql_queries.h"
#include "macros.h"
#include "struct.h"
#include "Test.h"

#define RUN_TEST_N_TIMES 5

int testComparePatterns(){
    int nrErr = 0,ret;

    ASSERT_EQ(comparePatterns("a.b.c", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("a.b.c", "a.d.c",0), 0);
    ASSERT_EQ(comparePatterns("a.b.c", "a.b.c.a",0), 0);
    ASSERT_EQ(comparePatterns("a.b.c.a", "a.b.c",0), 0 );
    ASSERT_EQ(comparePatterns("a.?.c", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("a.*.c", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("*.c", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("a.b.?", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("a.b.*", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("a.*", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("*", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("a.b.c.*", "a.b.c",0), 1);
    ASSERT_EQ(comparePatterns("a.b.c.*.a", "a.b.c",0), 0);
    ASSERT_EQ(comparePatterns("a.alaMaKota.c", "a.b.c",0), 0);
    ASSERT_EQ(comparePatterns("*.f", "a.b.c",0), 0);
    ASSERT_EQ(comparePatterns("?.#odd", "a.b.c",0), 0);
    ASSERT_EQ(comparePatterns("asd", "",0), 0);
    ASSERT_EQ(comparePatterns("a.?.*.?.e.f", "a.b.c.d.e.f", 0), 1);
    ASSERT_EQ(comparePatterns("a.?.*.?.e.f", "a.b.c.e.f", 0), 1);
    ASSERT_EQ(comparePatterns("a.?.*.?.e.f", "a.b.e.f", 0), 0);

    ASSERT_EQ(comparePatterns("a.*.c.e.f", "a.b.c.b.e.b.f",0), 0)
    ASSERT_EQ(comparePatterns("a.b.*.c.d", "a.b.b.c.b.c.d", 0), 1)

    TEST_INFO("ComparePatterns")
    fflush(stdout);
    return nrErr;
}
int testGetNextNodeIndex(){
    int nrErr = 0,ret;

    ASSERT_EQ(getNextNodeIndex("ala.ma.kota"), 4);
    ASSERT_EQ(getNextNodeIndex("ala"), -1111111);

    TEST_INFO("GetNextNodeIndex")
    return nrErr;
}
int testCompareStringsToTheFirstDot(){
    int nrErr = 0,ret, dotIndex;

    ASSERT_EQ(compareStringsToTheFirstDot("b.c", "d.c", 0),0);     //   ASSERT_EQ(dotIndex, 3);
    ASSERT_EQ(compareStringsToTheFirstDot("ala.ma.kota", "ala.ma.kota", &dotIndex), 1);        ASSERT_EQ(dotIndex, 3);
    ASSERT_EQ(compareStringsToTheFirstDot("ala.ma.kota", "ala.nie.ma.kota", &dotIndex), 1);    ASSERT_EQ(dotIndex, 3);
    ASSERT_EQ(compareStringsToTheFirstDot("ala", "ala", &dotIndex), 1);                        ASSERT_EQ(dotIndex, -1111111);
    ASSERT_EQ(compareStringsToTheFirstDot("ala.ma.kota", "ala", 0), 1);
    ASSERT_EQ(compareStringsToTheFirstDot("ala", "al", &dotIndex),  0);
    ASSERT_EQ(compareStringsToTheFirstDot("ala", "alala", &dotIndex), 0);
    ASSERT_EQ(compareStringsToTheFirstDot("ala", "", &dotIndex), SQLxD_EMPYT_STRING);
    ASSERT_EQ(compareStringsToTheFirstDot("aba", "ada", &dotIndex), 0);
    ASSERT_EQ(compareStringsToTheFirstDot("", "ala", &dotIndex), SQLxD_EMPYT_STRING);

    TEST_INFO("CompareStringsToTheFirstDot");
    fflush(stdout);
    return nrErr;
}
int testIsAllowedCharInNodeName(){
    int nrErr = 0,ret;

    ASSERT_EQ(isAllowedCharInNodeName('a'), 1);
    ASSERT_EQ(isAllowedCharInNodeName('z'), 1);
    ASSERT_EQ(isAllowedCharInNodeName('g'), 1);
    ASSERT_EQ(isAllowedCharInNodeName('0'), 1);
    ASSERT_EQ(isAllowedCharInNodeName('4'), 1);
    ASSERT_EQ(isAllowedCharInNodeName('5'), 1);

    ASSERT_EQ(isAllowedCharInNodeName('A'), 0);
    ASSERT_EQ(isAllowedCharInNodeName('Z'), 0);
    ASSERT_EQ(isAllowedCharInNodeName('-'), 0);
    ASSERT_EQ(isAllowedCharInNodeName('*'), 1);
    ASSERT_EQ(isAllowedCharInNodeName(' '), 0);
    ASSERT_EQ(isAllowedCharInNodeName('\0'), 0);
    ASSERT_EQ(isAllowedCharInNodeName('\n'), 0);
    ASSERT_EQ(isAllowedCharInNodeName('*'), 1);
    ASSERT_EQ(isAllowedCharInNodeName('#'), 1);
    ASSERT_EQ(isAllowedCharInNodeName('?'), 1);

    TEST_INFO("IsAllowedCharInNodeName()");
    fflush(stdout);
    return nrErr;
}
int testIsAllowedCommonCharInNodeName(){
    int nrErr = 0,ret;

    ASSERT_EQ(isAllowedCommonCharInNodeName('a'), 1);
    ASSERT_EQ(isAllowedCommonCharInNodeName('z'), 1);
    ASSERT_EQ(isAllowedCommonCharInNodeName('g'), 1);
    ASSERT_EQ(isAllowedCommonCharInNodeName('0'), 1);
    ASSERT_EQ(isAllowedCommonCharInNodeName('4'), 1);
    ASSERT_EQ(isAllowedCommonCharInNodeName('5'), 1);

    ASSERT_EQ(isAllowedCommonCharInNodeName('A'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('Z'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('-'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('*'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName(' '), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('\0'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('\n'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('*'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('#'), 0);
    ASSERT_EQ(isAllowedCommonCharInNodeName('?'), 0);

    TEST_INFO("IsAllowedCommonCharInNodeName()");
    fflush(stdout);
    return nrErr;
}
int testCheckTheCorrectnessOfPattern(){
    int nrErr = 0,ret;

    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "#"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "#ala.#"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "#ala.?"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "?"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "*"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.*.?.hello"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, ".people.*.?.hello"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people..*.?.hello"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.person..hobbies"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.*.?.#hello"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.*.?.#hello.#"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.person.hobbies"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "PEOPLE.PERSON.hOBBies"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.person.?"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.person.*"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.person.#"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.*.aw?.hello.#"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.*.#aw.?.hello"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.*.#?.hello"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "people.*.?.hello."), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.*b.*.c.d.#"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.*.b.*.c.d.#"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.?b.?.c.d.#"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.?.b.?.c.d.#"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.?.b.*.c.d.#"), 0);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.?.b.*.c.d.?#"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.?.b.*.c.d.#?"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, "a.?.b.*.c.d.#ads.#ad"), SQLxD_PATTERN_FAULTY);
    ASSERT_EQ(checkTheCorrectnessOfPattern(0, ""), SQLxD_EMPYT_STRING);

    TEST_INFO("CheckTheCorrectnessOfPattern")
    fflush(stdout);
    return nrErr;
}
int testAddResolvePatternNodeList(){
    int nrErr = 0,ret, i=0;

    while (i++ < RUN_TEST_N_TIMES){
        NodeList* nodeList = createNodeList();
        ASSERT_EQ(addNodeToNodeList(0, nodeList, "NoDe1", "AlIaS1"), 0);    ASSERT_EQ(addNodeToNodeList(0, nodeList, "node2", "alias2"), 0);
        ASSERT_EQ(nodeList->nList, 2);
        ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias1", "pattern1"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias2", "pattern1"), 0);
        ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias1", "pattern2"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias2", "pattern2"), 0);

        prepareToReadXML(nodeList);

        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern1"), 0);    ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern2"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern2"), 0);   ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern3"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern4"), 0);    ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern5"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern6"), 0);    ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern7"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern8"), 0);    ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern9"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern10"), 0);   ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern11"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern12"), 0);   ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern13"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern14"), 0);   ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern15"), 0);

        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern1"), 0);   ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 0, "resolvePattern2"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[1], 0, "resolvePattern1"), 0);    ASSERT_EQ(addResolvePatternToNode(nodeList->list[1], 0, "resolvePattern2"), 0);

        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 0), 15);
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 1), 0);
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 2), 0);
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 3), SQLxD_INDEX_OF_BOUND);
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[1], 0), 2);

        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 1, "resolvePattern1"), 0);    ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 1, "resolvePattern2"), 0);
        ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 2, "resolvePattern1"), 0);    ASSERT_EQ(addResolvePatternToNode(nodeList->list[0], 2, "resolvePattern2"), 0);
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 1), 2);
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 2), 2);

        //printNodeList(nodeList);
        removeNodeList(nodeList);
    }
    TEST_INFO("AddResolvePatternNodeList")
    fflush(stdout);
    return nrErr;
}
int testAddPatterNodeList(){
    int nrErr = 0,ret, i=0;

    while (i++ < RUN_TEST_N_TIMES){
        NodeList* nodeList = createNodeList();
        ASSERT_EQ(addNodeToNodeList(0, nodeList, "TooLongNodeName____aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.sssssssssssssssssssssssssssssss.sssssssssssssssssssssssssssssssssssssss","alias"), SQLxD_PATTERN_TOO_LONG);
        ASSERT_EQ(addNodeToNodeList(0, nodeList, "NoDe1", "AlIaS1"), 0);    ASSERT_EQ(addNodeToNodeList(0, nodeList, "node2", "alias2"), 0);
        ASSERT_EQ(addNodeToNodeList(0, nodeList, "node1", "alias1"), SQLxD_ALIAS_NOT_UNIQUE);
        ASSERT_EQ(nodeList->nList, 2);

        ASSERT_EQ(addNodeToNodeList(0, nodeList, "node3", "alias3"), 0);    ASSERT_EQ(addNodeToNodeList(0, nodeList, "node4", "alias4"), 0);    ASSERT_EQ(addNodeToNodeList(0, nodeList, "node5", "alias5"), 0);
        ASSERT_EQ(addNodeToNodeList(0, nodeList, "node6", "alias6"), 0);    ASSERT_EQ(addNodeToNodeList(0, nodeList, "node7", "alias7"), 0);    ASSERT_EQ(addNodeToNodeList(0, nodeList, "node8", "alias8"), 0);
        ASSERT_EQ(addNodeToNodeList(0, nodeList, "node9", "alias9"), 0);    ASSERT_EQ(addNodeToNodeList(0, nodeList, "node10", "alias10"), 0);  ASSERT_EQ(addNodeToNodeList(0, nodeList, "node11", "alias11"), 0);
        ASSERT_EQ(addNodeToNodeList(0, nodeList, "node12", "alias12"), 0);  ASSERT_EQ(addNodeToNodeList(0, nodeList, "node13", "alias13"), 0);  ASSERT_EQ(addNodeToNodeList(0, nodeList, "node14", "alias14"), 0);
        ASSERT_EQ(nodeList->nList, 14);

        ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias1", "pattern1"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias2", "pattern1"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias3", "pattern1"), 0);
        ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias4", "pattern1"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias5", "pattern1"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias6", "pattern1"), 0);
        ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias7", "pattern1"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias8", "pattern1"), 0);
        ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias1", "pattern1"), 0);    ASSERT_EQ(addPatternToNodeList(0, nodeList, "alias6", "pattern1"), 0);

        prepareToReadXML(nodeList);

        removeNodeList(nodeList);
    }
    TEST_INFO("AddPatterNodeList")
    fflush(stdout);
    return nrErr;
}
int testListString(){
    int nrErr = 0, ret, i=0;

    while (i++ < RUN_TEST_N_TIMES){
        List_string* list = createList_string(2);
        ASSERT_EQ(list->allocSize, 2 );
        ASSERT_EQ(list->size, 0);
        addToList_string(list, "str1");    addToList_string(list, "str2");    addToList_string(list, "str3");

        ASSERT_EQ(list->size, 3);
        addToList_string(list, "str4");    addToList_string(list, "str5");    addToList_string(list, "str6");    addToList_string(list, "str7");
        addToList_string(list, "str8");    addToList_string(list, "str9");    addToList_string(list, "str10");    addToList_string(list, "str11");
        addToList_string(list, "str12");    addToList_string(list, "str13");    addToList_string(list, "str14");    addToList_string(list, "str15");
        ASSERT_EQ(list->size, 15);

        ASSERT_EQ(addUniqueToList_string(list, "str16"),0);    addUniqueToList_string(list, "str17");    addUniqueToList_string(list, "str18");
        ASSERT_EQ(list->size, 18);

        ASSERT_EQ(addUniqueToList_string(list, "str14"),0);
        ASSERT_EQ(addUniqueToList_string(list, "str15"),0);
        ASSERT_EQ(addUniqueToList_string(list, "Str14"),0);
        ASSERT_EQ(addUniqueToList_string(list, "sTr15"),0);
        ASSERT_EQ(list->size, 18);

        ASSERT_EQ(containsList_string(list, "str5"), 4);
        ASSERT_EQ(containsList_string(list, "Str5"), 4);
        ASSERT_EQ(containsList_string(list, "Str513"), -248);

        removeList_string(list);
    }
    TEST_INFO("ListString");
    fflush(stdout);
    return nrErr;
}
int testContainsList_string(){
    int nrErr = 0,ret,i=0;

    while (i++ < RUN_TEST_N_TIMES){
        List_string* list = createList_string(5);
        addToList_string(list,"AbCdEF");
        addToList_string(list,"alamakota");
        addToList_string(list,"NieMa");
        addToList_string(list,"takiSobieNapis");
        ASSERT_EQ(list->size, 4);

        ASSERT_EQ(containsList_string(list,"abcdef"), 0);
        ASSERT_EQ(containsList_string(list,"alamakota"), 1);
        ASSERT_EQ(containsList_string(list,"niema"), 2);
        ASSERT_EQ(containsList_string(list,"takiSobieNapis"), 3);
        ASSERT_EQ(containsList_string(list,"listaNieMaTakiegoNapisu"), -248);
    }
    TEST_INFO("ContainsList_string")
    fflush(stdout);
    return nrErr;
}
int testGetNodeByAlias(){
    int nrErr = 0,ret,i=0;

    while (i++ < RUN_TEST_N_TIMES){
        NodeList* nodeList = createNodeList();
        addNodeToNodeList(0, nodeList, "NoDe1","AliaS1");
        addNodeToNodeList(0, nodeList, "node2","AliaS2");
        addNodeToNodeList(0, nodeList, "NoDe3","alias3");
        ASSERT_EQ(nodeList->nList, 3);

        ASSERT_EQ(strcmp(getNodeByAlias(nodeList, "AliaS1")->name, "node1"), 0);
        ASSERT_EQ(strcmp(getNodeByAlias(nodeList, "alias2")->name, "node2"), 0);
        ASSERT_EQ(strcmp(getNodeByAlias(nodeList, "AliaS3")->name, "node3"), 0);
        ASSERT_EQ((int)getNodeByAlias(nodeList, "nieMaTakiego"), 0);

        removeNodeList(nodeList);
    }
    TEST_INFO("GetNodeByAlias")
    fflush(stdout);
    return nrErr;
}
int testGetSQLTableName(){
    int nrErr = 0,ret,i=0;

    while (i++ < RUN_TEST_N_TIMES){
        Node* node1 = createNode("pEopLe", "pErsOn");
        Node* node2 = createNode("pEoPle.person", "person" );
        Node* node3 = createNode("people.?.hobbies", "hobbies");
        Node* node4 = createNode("people.*.hobbies", "hobbies");
        Node* node5 = createNode("people.?", "hobbies");
        Node* node6 = createNode("people.*", "hobbies");

        ASSERT_EQ(strcmp(getSQLTableName(node1), XML_TABLE_NODE_PREFIX "people" ), 0)
        ASSERT_EQ(strcmp(getSQLTableName(node2), XML_TABLE_NODE_PREFIX "people_person" ), 0)
        ASSERT_EQ(strcmp(getSQLTableName(node3), XML_TABLE_NODE_PREFIX "people__q__hobbies" ), 0)
        ASSERT_EQ(strcmp(getSQLTableName(node4), XML_TABLE_NODE_PREFIX "people__s__hobbies" ), 0)
        ASSERT_EQ(strcmp(getSQLTableName(node5), XML_TABLE_NODE_PREFIX "people__q_" ), 0)
        ASSERT_EQ(strcmp(getSQLTableName(node6), XML_TABLE_NODE_PREFIX "people__s_" ), 0)

        Node* node7 = createNode("pEoPle.person", "pErsOn1");
        Node* node8 = createNode("hobbies.hobby", "hobby");
        addPtrNodeToFollowedList(node7, node8);
        node8->preceded = node7;

        ASSERT_EQ(strcmp(getSQLTableName(node7), XML_TABLE_NODE_PREFIX "people_person" ), 0)
        ASSERT_EQ(strcmp(getSQLTableName(node8), XML_TABLE_NODE_PREFIX "_person_hobbies_hobby"), 0) //        printf("node8 %s\n", node8->sqlTableName);

        remove_Node(node1);    remove_Node(node2);    remove_Node(node3);    remove_Node(node4);
        remove_Node(node5);    remove_Node(node6);    remove_Node(node7);    remove_Node(node8);
    }
    TEST_INFO("GetSQLTableName");
    fflush(stdout);
    return nrErr;
}
int testGetPtrToLastNode(){
    int nrErr = 0,ret;

    ASSERT_EQ( strcmp("c", getPtrToLastNode("a.b.c") ), 0)
    ASSERT_EQ( strcmp("a", getPtrToLastNode("a") ), 0)
    ASSERT_EQ( strcmp("asdawdc", getPtrToLastNode("a.b.asdawdc") ), 0)
    ASSERT_EQ( strcmp("aawdwd", getPtrToLastNode("aawdwd") ), 0)
    ASSERT_EQ( strcmp("c", getPtrToLastNode("a.b.c") ), 0)
    ASSERT_EQ( strcmp("", getPtrToLastNode("") ), 0)
    ASSERT_EQ( (int)getPtrToLastNode(0), 0)

    TEST_INFO("GetPtrToLastNode");
    fflush(stdout);
    return nrErr;
}
int testGetXmlDocName(){
    int nrErr = 0,ret,i=0;

    while (i++ < RUN_TEST_N_TIMES){
        Node* node1 = createNode("pEopLe", "pErsOn");
        Node* node2 = createNode("pEoPle.person", "person" );
        Node* node3 = createNode("people.?.hobbies", "hobbies");
        Node* node4 = createNode("pEoPle.person", "pErsOn");
        Node* node5 = createNode("person.hobbies", "hobbies");
        addPtrNodeToFollowedList(node4, node5);
        node5->preceded = node4;

        ASSERT_EQ(strcmp(getXmlDocName(node1), "people"), 0)
        ASSERT_EQ(strcmp(getXmlDocName(node2), "people"), 0)
        ASSERT_EQ(strcmp(getXmlDocName(node3), "people"), 0)
        node4->xmlDocName = getXmlDocName(node4);
        ASSERT_EQ(strcmp(node4->xmlDocName, "people"), 0)
        ASSERT_EQ(strcmp(getXmlDocName(node5), "people"), 0)

        remove_Node(node1);    remove_Node(node2);    remove_Node(node3);    remove_Node(node4);
    }

    TEST_INFO("GetXmlDocName");
    fflush(stdout);
    return nrErr;
}
int testResolvePatterns(){
    int nrErr = 0,ret,i=0;

    while (i++ < RUN_TEST_N_TIMES){
        NodeList* nodeList = createNodeList();
        addNodeToNodeList(0, nodeList, "people.person", "person");
        addPatternToNodeList(0, nodeList, "person", "*");
        addPatternToNodeList(0, nodeList, "person", "firstname");
        addPatternToNodeList(0, nodeList, "person", "lastname");
        addPatternToNodeList(0, nodeList, "person", "?.city");
        addPatternToNodeList(0, nodeList, "person", "?.street");
        addPatternToNodeList(0, nodeList, "person", "*.bldg");
        addPatternToNodeList(0, nodeList, "person", "*.flat");

        prepareToReadXML(nodeList);

        addResolvePatternToNode(nodeList->list[0], 0, "person.id");    addResolvePatternToNode(nodeList->list[0], 0, "person.firstname");    addResolvePatternToNode(nodeList->list[0], 0, "person.firstname.#");
        addResolvePatternToNode(nodeList->list[0], 0, "person.lastname");    addResolvePatternToNode(nodeList->list[0], 0, "person.address");    addResolvePatternToNode(nodeList->list[0], 0, "person.address.city");
        addResolvePatternToNode(nodeList->list[0], 0, "person.sth.city");    addResolvePatternToNode(nodeList->list[0], 0, "person.address.street");    addResolvePatternToNode(nodeList->list[0], 0, "person.address.house");
        addResolvePatternToNode(nodeList->list[0], 0, "person.address.house.bldg");    addResolvePatternToNode(nodeList->list[0], 0, "person.address.bldg");    addResolvePatternToNode(nodeList->list[0], 0, "person.bldg");
        addResolvePatternToNode(nodeList->list[0], 0, "person.address.house.flat");    addResolvePatternToNode(nodeList->list[0], 0, "person.address.flat");

        resolvePatterns(0, nodeList);

        ASSERT_EQ(nodeList->list[0]->nPattern, 8); /// 7 + 'all_nodes' = 8
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 0), 14)   ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 1), 14)
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 2), 1)    ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 3), 1)
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 4), 2)    ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 5), 1)
        ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 6), 3)    ASSERT_EQ(getResolvePatternListSize(nodeList->list[0], 7), 2)

        ASSERT_EQ(strcmp(getResolvesPatternList(nodeList, "person", "firstname")->tab[0], "person.firstname"), 0);
        ASSERT_EQ(strcmp(getResolvesPatternList(nodeList, "person", "?.city")->tab[0], "person.address.city"), 0);
        ASSERT_EQ(strcmp(getResolvesPatternList(nodeList, "person", "*.flat")->tab[0], "person.address.house.flat"), 0);
    //    addResolvePatternToNode(nodeList->list[0], 0, "");
    //    addResolvePatternToNode(nodeList->list[0], 0, "");
    //    addResolvePatternToNode(nodeList->list[0], 0, "");

    //    printNodeList(nodeList);
    }
    TEST_INFO("ResolvePatterns")
    fflush(stdout);
    return nrErr;
}
int testGetPtrToSecondNode(){
    int nrErr = 0,ret;

    ASSERT_EQ(strcmp(getPtrToSecondNode("abc.def.gh"), "def.gh"), 0);
    ASSERT_EQ(strcmp(getPtrToSecondNode("a.b.c"), "b.c"), 0);
    ASSERT_EQ(strcmp(getPtrToSecondNode("a.b"), "b"), 0);
    ASSERT_EQ(strcmp(getPtrToSecondNode("a"), "a"), 0);

    TEST_INFO("GetPtrToSecondNode")
    fflush(stdout);
    return nrErr;
}
int testListPtrNode(){
    int nrErr = 0,ret,i=0;

    while (i++ < RUN_TEST_N_TIMES){
        Node* node1 =  createNode("name1","alias1");
        Node* node2 =  createNode("name2","alias2");
        Node* node3 =  createNode("name3","alias3");
        Node* node4 =  createNode("name4","alias4");
        Node* node5 =  createNode("name5","alias5");
        Node* node6 =  createNode("name6","alias6");
        Node* node7 =  createNode("name7","alias7");
        Node* node8 =  createNode("name8","alias8");

        addPtrNodeToFollowedList(node1, node2);    addPtrNodeToFollowedList(node1, node3);    addPtrNodeToFollowedList(node1, node4);
        addPtrNodeToFollowedList(node1, node5);    addPtrNodeToFollowedList(node1, node6);    addPtrNodeToFollowedList(node1, node7);    addPtrNodeToFollowedList(node1, node8);

        ASSERT_EQ(node1->followed->nList, 7)
        ASSERT_EQ((int)getNodefromFollowedList(node1,0), (int)node2);

     //   printNode(node1);

        remove_Node(node1);
        remove_Node(node2);
        remove_Node(node3);
        remove_Node(node4);
        remove_Node(node5);
        remove_Node(node6);
        remove_Node(node7);
        remove_Node(node8);
    }

    TEST_INFO("ListPtrNode")
    fflush(stdout);
    return nrErr;
}

void StructTest(){
    PRINT_STARTS
    int nr = 0;

    sqlite3* db;
	sqlite3_open(0, &db); // init sqlite memory pool

    nr += testCompareStringsToTheFirstDot();
    nr += testComparePatterns();
    nr += testGetNextNodeIndex();
    nr += testIsAllowedCharInNodeName();
    nr += testIsAllowedCommonCharInNodeName();

    nr += testListString();
    nr += testContainsList_string();

    nr += testAddPatterNodeList();
    nr += testAddResolvePatternNodeList();
    nr += testCheckTheCorrectnessOfPattern();
    nr += testGetXmlDocName();
    nr += testGetSQLTableName();
    nr += testGetNodeByAlias();
    nr += testGetPtrToLastNode();
    nr += testGetPtrToSecondNode();
    nr += testListPtrNode();
    nr += testResolvePatterns();

    if (db != 0){
    	sqlite3_close(db);
    }

    PRINT_STARTS
    printf("*          STRUCT TEST: %s, number of errors %d              *\n",
    		nr == 0 ? "Passed" : "Not passed", nr);
    PRINT_STARTS
    printf("\n");
    fflush(stdout);
}
