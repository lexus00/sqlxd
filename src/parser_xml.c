#include <parser.h>
#include <macros.h>

#include <libxml/parser.h>
#include <libxml/tree.h>

#define _AF ASSERT_FALSE


SQLxD_PRIVATE int sqlxd_query_xml_document_check_followed(Node* node, xmlNode* cur_node,
		long long int dbSuperNodeId, ROW* row);

SQLxD_PRIVATE int sqlxd_add_xml_attribute(ROW* row, xmlAttr* attr, char* parent_col_name){
	if ( !(attr->children && attr->children->content) )
		return 0;
	char* path = (char*) malloc (MAX_COLUMN_SIZE);
	sprintf(path, "%s.#%s", parent_col_name, (char*) attr->name);
	row_add(row, path, (char*)attr->children->content);
    sprintf(path, "%s.#", path);
	row_add(row, path, (char*)attr->children->content);
	free(path);
	return 0;
}

SQLxD_PRIVATE int sqlxd_collect_xml_document_data(Node* node, xmlNode* cur_node, ROW* row,
		char* cur_path, int level) {
	if ( ! cur_node )
		return 0;

	xmlAttr* attr = cur_node->properties;
	while ( attr ){
		sqlxd_add_xml_attribute(row, attr, cur_path);
		attr = attr->next;
	}

	xmlNode* a_node = cur_node->children;
	while ( a_node ){
		if ( a_node->type == XML_ELEMENT_NODE ){
			char* a_path = (char*) malloc (MAX_COLUMN_SIZE);
			sprintf(a_path, "%s.%s", cur_path, (const char*) a_node->name);
			if( !row_contains(row, a_path) ){
				row_add(row, a_path, NULL);
				sqlxd_collect_xml_document_data(node, a_node, row, a_path, level+1);
			}
			free(a_path);
		} else if ( a_node->type == XML_TEXT_NODE && isNotEmptyString((char*)a_node->content) ){
			char* a_path = (char*) malloc (MAX_COLUMN_SIZE);
			sprintf(a_path, "%s.#", cur_path);
			row_add(row, cur_path, (char*)a_node->content);
			row_add(row, a_path, (char*)a_node->content);
			free(a_path);
		}

		a_node = a_node->next;
	}

	if (level == 0){
		row->on_collect_data(node, row);
	}
	return 0;
}

SQLxD_PRIVATE int sqlxd_query_xml_document(Node* node, xmlNode* a_node, int shift,
		int starMode, ROW* row) {
	if (!node || shift < 0) {
		if (shift < 0)
			printf("shift < 0 ");
		return -1;
	}
	if (!a_node)
		return 0;

	deb_in("sqlxd_query_xml_document")

	int typeOfNextNode = 0; /// 0 - normal id(e.g. person), 1 - '?', 2 - '*'
	if (node->name[shift] == '?')
		typeOfNextNode = '?';
	else if (node->name[shift] == '*')
		typeOfNextNode = '*';

	int rc, incSuperNodeId = node->dbSuperNodeId == 0;

	xmlNode *cur_node = NULL;
	int newShift = getNextNodeIndex(node->name + shift) + shift; /// if newShift < 0 the user pattern is satisfied
	int cur_match;
	for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
		if (cur_node->type != XML_ELEMENT_NODE)
			continue;
		cur_match = compareStringsToTheFirstDot((char*) cur_node->name,
				node->name + shift, 0) == 1;

		if (incSuperNodeId)
			++node->dbSuperNodeId;

		if (newShift < 0 && (cur_match || typeOfNextNode > 0)) {
			++node->dbNodeId;
			if (node->dbDocId > 0)
				_AF( sqlxd_collect_xml_document_data(node, cur_node, row, (char*)cur_node->name, 0) )
			_AF( sqlxd_query_xml_document_check_followed(node, cur_node, node->dbNodeId, row))
		}

		if (typeOfNextNode == '?' || cur_match) {
			if (starMode == 1 && cur_match)
				_AF(sqlxd_query_xml_document(node, cur_node->children, shift, 1, row))

			if (newShift > 0)
				_AF(sqlxd_query_xml_document(node, cur_node->children, newShift, starMode, row))

		} else if (typeOfNextNode == '*' && newShift > 0) {
			_AF(sqlxd_query_xml_document(node, cur_node->children, newShift, 1, row))
		} else if (starMode == 1) {
			_AF(sqlxd_query_xml_document(node, cur_node->children, shift, 1, row))
		}
	}

	deb_out("sqlxd_query_xml_document")
	return 0;
}

SQLxD_PRIVATE int sqlxd_query_xml_document_check_followed(Node* node, xmlNode* cur_node,
		long long int dbSuperNodeId, ROW* row) {
	if (node->followed) {
		int i, rc;
		Node* ptr = NULL;
		for (i = 0; i < node->followed->nList; ++i) {
			ptr = getNodefromFollowedList(node, i);
			ptr->dbSuperNodeId = dbSuperNodeId;
			_AF( sqlxd_query_xml_document(ptr, cur_node->children, 0, 0, row) )
		}
	}
	return 0;
}

int sqlxd_process_xml_document(const void* blob, int length, Node* node, ROW* row) {
	xmlDoc* doc = xmlReadMemory(blob, length, "noname.xml", NULL, 0);
	if (doc == 0)
		return SQLxD_FILE_CANNOT_BE_READ;

	xmlNode *root_element = xmlDocGetRootElement(doc);
	int rc = sqlxd_query_xml_document(node, root_element, 0, 0, row);
	xmlFreeDoc(doc);
	return rc;
}
