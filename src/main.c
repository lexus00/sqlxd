#ifndef SQLXD_SHELL

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>

#include <sqlite3.h>

void queryOnPeopleXML(sqlite3* db);
void queryOnPayTableXML(sqlite3* db);
extern const char *sqlite3_column_name(sqlite3_stmt*, int N);
extern int __sqlxd_execute(sqlite3* db, const char* sql, void (*onSQLITE_ROW)(sqlite3* db, sqlite3_stmt *stmt, void* ptr), void* ptr, int mode);

int wasPrintedHeader = 0;
void printResult(sqlite3* db, sqlite3_stmt *stmt, void* ptr){
    int i, nrOfColumns = sqlite3_column_count(stmt);
    char* delimeter ="|";
    if (!wasPrintedHeader){
        printf("%s", sqlite3_column_name(stmt, 0));
        for(i=1; i<nrOfColumns; i++)
            printf("%s%s",delimeter, sqlite3_column_name(stmt, i));
        printf("\n");
        wasPrintedHeader = 1;
    }

    for(i=0; i<nrOfColumns; i++){
        char* txt = (char*) sqlite3_column_text(stmt, i);
        txt = txt == NULL ? "" : txt;
        printf("%s%s", txt, delimeter);
    }
    printf("\n");
}

int execSqlCommand(sqlite3* db,const char* sql){
    int tmp = wasPrintedHeader;
    printf("\nexec '%s'\n\n",sql);
    int rc = __sqlxd_execute(db, sql, printResult, 0, 0);
    wasPrintedHeader = tmp;
    return rc;
}

void removeFileIfExists(const char* filename) {
	if (access(filename, F_OK) != -1) {
		remove(filename);
	}
}


int main(int argc, char **argv){
    sqlite3* db;
	if (sqlite3_open("database.db", &db) != SQLITE_OK){ // in memmory database
		printf("Cannot open database\n");
		return -1;
	}
//    sqlxd_load_xml(db, "test/data/people.xml");
//
//    execSqlCommand(db, "SELECTXML AVG(CAST(person.#id as INTEGER)) "
//    		"FROM people.person as person");
//
//    execSqlCommand(db, "SELECTXML person.#id.#, person.#missing, person.firstname.#, "
//    		"person.lastname.#, person.*.city, hobby.# FROM people.person as person "
//    		"NATURAL JOIN person.hobbies.hobby as hobby");

	sqlxd_load_json(db, "test/data/invoice0001.json");
//	execSqlCommand(db, "SELECTXML inv.number as invoice_number, CAST(prod.id as INTEGER) as product_id, "
//			"prod.code, CAST(prod.totalCost as REAL) as totalCost, CAST(prod.amount as INTEGER) as amount "
//			"FROM invoice as inv NATURAL JOIN inv.product as prod");

	execSqlCommand(db, "SELECTXML inv.* FROM invoice as inv");
	execSqlCommand(db, "SELECTXML inv.*, prod.*  FROM invoice as inv NATURAL JOIN inv.products as prod");

    sqlite3_close(db);
    fflush(stdout);
    return 0;
}

#endif // SQLXD_SHELL
