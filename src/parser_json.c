#include <stdio.h>

#include <parser.h>
#include <macros.h>
#include <json.h>
#include <struct.h>
#include <utils.h>

#define _AF ASSERT_FALSE

SQLxD_PRIVATE int sqlxd_query_json_document(Node* node, json_value* a_node,
		/*char* a_node_name,*/ int shift, int starMode, ROW* row);

static int is_base_type(json_type type){
    switch(type){
        case json_string:
        case json_integer:
        case json_double:
        case json_boolean:
        case json_null:
        case json_none:
            return 1;

        default:
            /// json_object
            /// json_array
            return 0;
    }
}

static char* get_base_value(json_value* js_value){
    char* buf;
    switch(js_value->type){
        case json_string:
            return copyStr(js_value->u.string.ptr);
        case json_integer:
            buf = malloc(20);
            sprintf(buf, "%ld", (long int)js_value->u.integer);
            return buf;
        case json_double:
            buf = malloc(20);
            sprintf(buf, "%f", js_value->u.dbl);
            return buf;
        case json_boolean:
            buf = malloc(6);
            sprintf(buf, "%s", js_value->u.boolean ? "true" : "false");
            return buf;
        case json_null:
            buf = malloc(5);
            sprintf(buf, "NULL");
            return buf;
        case json_none:
            return "";
        default:
            return 0;
    }
}

SQLxD_PRIVATE int sqlxd_collect_json_document_data(Node* node, json_value* a_node, ROW* row,
		char* a_path, int level){
	if ( !a_node ) return 0;
	int temp_db_node_id = node->dbNodeId;
	int is_object = a_node->type == json_object;
	int idx, length = is_object ? a_node->u.object.length : a_node->u.array.length;
	json_object_entry* obj_tab = a_node->u.object.values;
	json_value** array_tab = a_node->u.array.values;

	if ( is_base_type(a_node->type) ){
		char* cur_path = (char*) malloc (MAX_COLUMN_SIZE);
		sprintf(cur_path, "%s.#", a_path);
		char* value = get_base_value(a_node);
		row_add(row, a_path, value);
		row_add(row, cur_path, value);
	} else {
		for (idx=0; idx<length; idx++){
			json_value* cur_node = is_object ? obj_tab[idx].value : array_tab[idx];

			if (!is_object && is_base_type(cur_node->type) ){ // array with string values
				char* cur_path = (char*) malloc (MAX_COLUMN_SIZE);
				sprintf(cur_path, "%s.#", a_path);
				char* value = get_base_value(cur_node);
				row_add(row, a_path, value);
				row_add(row, cur_path, value);
			} else {
				char* cur_path = a_path;
				if ( is_object ){
					cur_path = (char*) malloc (MAX_COLUMN_SIZE);
					sprintf(cur_path, "%s.%s", a_path, copyStr(obj_tab[idx].name));
				}

				if( !row_contains(row, cur_path) || level <= 1){
					if (utils_cstr_contains(cur_path, '.'))
						row_add(row, cur_path, NULL);
					sqlxd_collect_json_document_data(node, cur_node, row, cur_path, level+1);
				}

				if ( is_object )
					free(cur_path);
			}
			if (level == 0 && !is_object){
				row->on_collect_data(node, row);
				++node->dbNodeId;
			}
		}
	}



	if (level == 0 && is_object){
//		row_print(row);
		row->on_collect_data(node, row);
	} else if (level == 0 && !is_object){
		node->dbNodeId = temp_db_node_id;
	}

	return 0;
}

SQLxD_PRIVATE int sqlxd_query_json_document_check_followed(Node* node, json_value* a_node,
		long long int dbSuperNodeId, ROW* row){
	if (node->followed) {
		int i, rc;
		Node* ptr = NULL;
		for (i = 0; i < node->followed->nList; ++i) {
			ptr = getNodefromFollowedList(node, i);

			if (a_node->type == json_object){
				ptr->dbSuperNodeId = dbSuperNodeId;
				_AF( sqlxd_query_json_document(ptr, a_node, 0, 0, row) )
			} else {
				int idx, length = a_node->u.array.length;
				for (idx=0; idx<length; idx++){
					ptr->dbSuperNodeId = dbSuperNodeId + idx;
					_AF( sqlxd_query_json_document(ptr, a_node->u.array.values[idx], 0, 0, row) )
				}
			}
		}
	}
	return 0;
}


SQLxD_PRIVATE int sqlxd_query_json_document(Node* node, json_value* a_node,
		/*char* a_node_name,*/ int shift, int starMode, ROW* row){
	if ( !a_node )				return 0;
	if ( !node || shift < 0)	return -1;
	deb_in("sqlxd_query_json_document")

	int rc, typeOfNextNode = 0; /// 0 - normal id(e.g. person), 1 - '?', 2 - '*'
	if (node->name[shift] == '?')
		typeOfNextNode = '?';
	else if (node->name[shift] == '*')
		typeOfNextNode = '*';

	int incSuperNodeId = node->dbSuperNodeId == 0;
	int newShift = getNextNodeIndex(node->name + shift) + shift;

	int is_object = a_node->type == json_object;
	int idx, length = is_object ? a_node->u.object.length : a_node->u.array.length;
	json_object_entry* obj_tab = a_node->u.object.values;

	if (!is_object){
		json_value* value = a_node->u.array.values[0];
		if (length > 0 && !is_base_type(value->type) )
			_AF( sqlxd_query_json_document(node, value, shift, starMode, row) );
		return 0;
	}

	for (idx=0; idx < length; idx++){
		json_value* cur_node = obj_tab[idx].value;
		if ( is_base_type(cur_node->type) )
			continue;

		char* cur_node_name = copyStr(obj_tab[idx].name);
		int cur_match = compareStringsToTheFirstDot(cur_node_name, node->name+shift, 0) == 1;
		if (incSuperNodeId)
			++node->dbSuperNodeId;

		if (newShift < 0 && (cur_match || typeOfNextNode > 0)) {
			++node->dbNodeId;
			if (node->dbDocId > 0)
				_AF( sqlxd_collect_json_document_data(node, cur_node, row, cur_node_name, 0) ) // uno raz
			_AF( sqlxd_query_json_document_check_followed(node, cur_node, node->dbNodeId, row))
		}

		if (typeOfNextNode == '?' || cur_match) {
			if (starMode == 1 && cur_match)
				_AF(sqlxd_query_json_document(node, cur_node, shift, 1, row))
			if (newShift > 0)
				_AF(sqlxd_query_json_document(node, cur_node, newShift, starMode, row))
		} else if (typeOfNextNode == '*' && newShift > 0) {
			_AF(sqlxd_query_json_document(node, cur_node, newShift, 1, row));
		} else if (starMode == 1) {
			_AF(sqlxd_query_json_document(node, cur_node, shift, 1, row));
		}
	}

	deb_out("sqlxd_query_json_document")
	return 0;
}


int sqlxd_process_json_document(const void* blob, int length, Node* node, ROW* row){
	json_value* doc = json_parse(blob, length);
	if ( doc == NULL)
		return SQLxD_FILE_CANNOT_BE_READ;

	json_object_entry* root = &doc->u.object.values[0];
	int rc, shift = getNextNodeIndex(node->name);
	if (shift < 0) {
		if (node->dbDocId > 0) {
			node->dbNodeId = 1;
			_AF(sqlxd_collect_json_document_data(node, root->value, row, (char*)root->name, 0))
		}
		_AF(sqlxd_query_json_document_check_followed(node, root->value, 1, row))
	} else {
		_AF(sqlxd_query_json_document(node, root->value, /*root->name,*/ shift, 0, row))
	}

	json_value_free (doc);
	return 0;
}
