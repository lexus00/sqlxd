
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "macros.h"


ArrayList* list_mk (void (*del_fn)(item), int (*eq_fn)(item, item)) {
	ArrayList* list = (ArrayList*) malloc (sizeof(ArrayList));
	list->arr = (item*) malloc (sizeof(item) * LIST_INITIAL_SIZE);
	list->size = 0;
	list->capacity = LIST_INITIAL_SIZE;
	list->del_fn = del_fn;
	list->eq_fn = eq_fn;
	return list;
}

void list_free(ArrayList* list) {
	uint32_t idx;
	for (idx=0; idx<list->size; ++idx)
		if (list->arr[idx] != NULL)
			list->del_fn( list->arr[idx] );
	free(list->arr);
	free(list);
}

void list_clean(ArrayList* list) {
	uint32_t idx;
	for (idx=0; idx<list->size; ++idx)
		if (list->arr[idx] != NULL){
			list->del_fn( list->arr[idx] );
			list->arr[idx] = NULL;
		}
	list->size = 0;
}


int __list_add (ArrayList* list, item it) {
	if (list == NULL || it == NULL)
		return SQLxD_NULL_ARGUMENT;

	if (list->size >= list->capacity){
		uint32_t ncapacity = list->capacity * LIST_GROWTH_RATE;
		item* temp = list->arr;
		list->arr = (item*) malloc (sizeof(item) * ncapacity);
		memcpy(list->arr, temp, sizeof(item) * ncapacity);
		list->capacity = ncapacity;
		free(temp);
	}

	list->arr[list->size] = it;
	++list->size;
	return 0;
}


int __list_index_of (ArrayList* list, item el) {
	if (list == NULL || el == NULL || list->eq_fn == NULL)
		return -SQLxD_NULL_ARGUMENT;
	uint32_t idx;
	for (idx=0; idx<list->size; ++idx){
		if ( list->eq_fn(list->arr[idx], el) )
			return idx;
	}
	return -1;
}

item list_get (ArrayList* list, uint32_t idx){
	if (list == NULL || list->size <= idx)
		return 0;
	return list->arr[idx];
}







