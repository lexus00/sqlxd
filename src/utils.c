
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 *  C_STR FUNCTIONS
 */

char* utils_cstr_to_lower (const char* str){
	char* temp = (char*) malloc (strlen(str) + 1);
	strcpy(temp, str);
	int i;
	for (i=0; temp[i]; ++i)
		temp[i] = tolower(temp[i]);
	return temp;
}

int utils_cstr_is_not_empty(const char* str) {
	int i;
	for (i = 0; i < strlen(str); i++)
		if (isgraph(str[i]))
			return i + 1;
	return 0;
}

char* utils_cstr_concat(const char* str1, const char* str2){
	char* temp = (char*) malloc (strlen(str1) + strlen(str2) + 1);
	strcpy(temp, str1);
	strcat(temp, str2);
	return temp;
}

char* utils_cstr_copy (const char* str){
	if (str == 0)
		return (char*) 0;
	char* temp = (char*) malloc (strlen(str) + 1);
	strcpy(temp, str);
	return temp;
}

char* utils_cstr_int_to_cstr (int num){
	char* str = malloc(10);
	sprintf(str, "%d", num);
	return str;
}

int utils_cstr_eq_fn(void* a, void* b){
	if (a == 0 || b == 0){
		return (a == 0 && b == 0) ? 1 : 0;
	}
	return (strcmp((const char*)a, (const char*)b) == 0) ? 1 : 0;
}

int utils_cstr_contains(const char* str, char c){
	int i;
	for (i=0; i<strlen(str); ++i)
		if (str[i] == '.')
			return 1;
	return 0;
}

/*
 *  PRINT FUNCTIONS
 */

void hmap_print(hashmap* map){
	static uint32_t it;
	printf("Hashmap [size=%d]\n", (int)map->size);
	for (it = 0; it<map->capacity; ++it) {
		if (map->map[it].v != NULL){
			printf("%s - %s\n", (char*)map->map[it].k, (char*)map->map[it].v);
			fflush(stdout);
		}
	}
}

void list_print(ArrayList* list, char* printf_template) {
	uint32_t idx;
	for (idx=0; idx<list->size; ++idx)
		printf(printf_template, list->arr[idx]);
}
