#include <stdio.h>

#include "macros.h"
#include "sqlite3.h"
#include "row.h"
#include "parser.h"
#include "sql_queries.h"
#include "struct.h"

#define MAX_INSERT_LENGTH 10000

static char sql[512];


SQLxD_PRIVATE int sqlite3_add_column(sqlite3* db, const char* tableName, const char* colName){
	char* sql = (char*) malloc (512);
	sprintf(sql, ADD_COLUMN, tableName, colName);
	return __sqlxd_execute(db, sql, 0, 0, 0);
}

SQLxD_PRIVATE void temp_add_to_sql(char* sql, const char* prefix, const char* val, const char* suffix){
	strcat(sql, prefix);
	strcat(sql, val);
	strcat(sql, suffix);
}

SQLxD_PRIVATE int sqlxd_on_collect_data(Node* node, void* _row){
	ROW* row = (ROW*) _row;
	List_string* list = sqlite3_column_names (row->db, node->sqlTableName);

	char* columns = (char*) malloc (MAX_INSERT_LENGTH);
	char* values = (char*) malloc (MAX_INSERT_LENGTH);
	sprintf(columns, INSERT_ROW_PREFIX, node->sqlTableName);
	sprintf(values, INSERT_ROW_VALUES_PREFIX,node->dbDocId,node->dbNodeId,node->dbSuperNodeId);

	static uint32_t it;
	for (it = 0; it < row->data->capacity; ++it)
		if (row->data->map[it].v != NULL){
			if ( !list_str_contains(list, row->data->map[it].k))
				sqlite3_add_column(row->db, node->sqlTableName, (char*)row->data->map[it].k);
			temp_add_to_sql(columns, ",[", (char*)row->data->map[it].k, "]");
			temp_add_to_sql(values, ",\"", (char*)row->data->map[it].v, "\"");
		}

	char* sql = NULL;
	int sql_len = strlen(columns) + strlen(values) + 4;
	if ( sql_len < MAX_INSERT_LENGTH){
		sql = columns;
	}	else {
		sql = (char*) malloc ( sql_len );
		strcpy(sql, columns);
	}
	strcat(sql, ")\n");
	strcat(sql, values);
	strcat(sql, ")");

	int rc = __sqlxd_execute(row->db, sql, 0, 0, 0);
	row->data = mk_hmap(str_hash_fn, str_eq_fn);
//	printf("rc = %d, row_id %d", rc, sqlite3_last_insert_rowid(row->db));
	return rc;
}



SQLxD_PRIVATE int _sqlxd_create_node_table(sqlite3* db, char* tableName){
    sprintf(sql, DROP_TABLE_IF_EXISTS, tableName);
    __sqlxd_execute(db, sql, 0, 0, 0);
    sprintf(sql, CREATE_NODE_TABLE, tableName);
    __sqlxd_execute(db, sql, 0, 0, 0);
    return 0;
}

SQLxD_PRIVATE void _sqlxd_fill_column_names (sqlite3* db, sqlite3_stmt *stmt, void *ptr){
    Node* node = (Node*)ptr;
    addResolvePatternToNode(node, 0, (const char*) sqlite3_column_text(stmt, 1));
}
SQLxD_PRIVATE void _sqlxd_last_update_date (sqlite3* db, sqlite3_stmt *stmt, void *ptr){
    int* lastDate = (int*) ptr;
    *lastDate =  sqlite3_column_int(stmt, 0);
}

SQLxD_PRIVATE int _sqlxd_set_basic_node_data(Node* node, int docLoadDate, sqlite3_int64 docID){
    int neetToBeUpdated = 0;
    node->dbSuperNodeId = 0;
    node->dbNodeId = 0;
    if ( node->newUpdateTime < docLoadDate)
        node->newUpdateTime = docLoadDate;

    if (node->lastUpdateTime >= docLoadDate)
        node->dbDocId = 0;
    else {
        node->dbDocId = docID;
        neetToBeUpdated = 1;
    }

    int i;
    if ( node->followed )
        for (i = 0; i < node->followed->nList; ++i)
            neetToBeUpdated += _sqlxd_set_basic_node_data(node->followed->list[i], docLoadDate, docID);
    return neetToBeUpdated;
}

SQLxD_PRIVATE void _sqlxd_for_each_document (sqlite3* db, sqlite3_stmt *stmt, void *ptr){
	Node* node = (Node*)ptr;
	int length = sqlite3_column_bytes(stmt, 0);
	int docLoadDate = sqlite3_column_int(stmt, 1);
	sqlite3_int64 docID = sqlite3_column_int(stmt, 2);
	int type = sqlite3_column_int(stmt, 3);

	// SKIP IF IT WAS ANALIZED IN BEFORE
	if (!_sqlxd_set_basic_node_data(node, docLoadDate, docID))
		return;

	ROW* row = row_mk(db, sqlxd_on_collect_data);
	if (type == XML_TYPE){
		sqlxd_process_xml_document(sqlite3_column_blob(stmt, 0), length, node, row);
	} else if(type == JSON_TYPE){
		sqlxd_process_json_document(sqlite3_column_blob(stmt, 0), length, node, row);
	}
}

int sqlxd_load_data_from_documents(NodeList* nodeList, sqlite3* db){
	deb_in("sqlxd_load_data_from_documents")

	Node* node = NULL;
	int i, rc;

	///get lastUpdatTime
	for (i=0; i<nodeList->nList; i++){
		node = nodeList->list[i];
		sprintf(sql, CT_SELECT_LAST_UPDATE, node->sqlTableName);
		__sqlxd_execute(db, sql, &_sqlxd_last_update_date, &node->lastUpdateTime, 0);
		if ( node->lastUpdateTime == 0){   // there is no array for this node
			_sqlxd_create_node_table(db, node->sqlTableName);
		}
	}

	for (i=0; i<nodeList->nList; i++){
		node = nodeList->list[i];
		if (node->preceded )
			continue;
		sprintf(sql, SELECT__STORE_TABLE, node->xmlDocName);
		ASSERT_FALSE(__sqlxd_execute(db, sql, &_sqlxd_for_each_document, nodeList->list[i], 0))
	}

	for (i=0; i<nodeList->nList; i++){
		node = nodeList->list[i];

		sprintf(sql, PRAGMA_TABLE_INFO, node->sqlTableName);
		__sqlxd_execute(db, sql, _sqlxd_fill_column_names, node, 0);
	}

	deb_out("sqlxd_load_data_from_documents")
	return sqlite3_errcode(db) ;
}
