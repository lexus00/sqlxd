
#include "row.h"
#include "struct.h"
#include <utils.h>
#include <stdio.h>


ROW* row_mk(struct sqlite3* db, int (*fun)(Node*, void*)){
	ROW* row = (ROW*) malloc (sizeof (ROW));
	row->db = db;
	row->data = mk_hmap(str_hash_fn, str_eq_fn);
	row->on_collect_data = fun;
	return row;
}

void row_free(ROW* row){

}

int row_contains(ROW* row, char* column_name){
	char* val = hmap_get(row->data, column_name);
	return val != NULL;
}

int row_add(ROW* row, char* k, char* v){
	if ( row_contains(row, k)){
		char* val = hmap_get(row->data, k);
		if ( !utils_cstr_eq_fn(val, "") ){
			return 0;
		}
	}
	char *v2 = v == NULL ? copyStr("") : copyStr(v);
	key_val_pair* pair = hmap_get_pair(row->data, k);
	if ( pair ){
		pair->v = v2;
		return 0;
	}
	return !hmap_add(row->data, copyStr(k), v2);
}

void row_print(ROW* row){
	static uint32_t it;
	printf("print row [size=%d]\n", (int)row->data->size);
	for (it = 0; it < row->data->capacity; ++it) {
		if (row->data->map[it].v != NULL)
			printf("%s - %s\n", (char*)row->data->map[it].k, (char*)row->data->map[it].v);
	}
}
