#ifndef __STRUCT_C__
#define __STRUCT_C__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>
#include <ctype.h>

#include "macros.h"
#include "struct.h"
#include "sql_queries.h"

#ifndef RUN_TEST
SQLxD_PRIVATE const char* getPtrToSecondNode(const char* str);
SQLxD_PRIVATE int checkTheCorrectnessOfPattern(void *pParse, const char* str);
SQLxD_PRIVATE int comparePatterns(const char* userPattern, const char* xmlPattern, int star);
SQLxD_PRIVATE void createSQLTableName(Node* node);
SQLxD_PRIVATE void createXmlDocName(Node* node);
SQLxD_PRIVATE Node* createNode(const char* name, const char* alias);
SQLxD_PRIVATE  void remove_Node(Node* node);
#endif // RUN_TEST

SQLxD_PRIVATE int comparePatterns(const char* userPattern, const char* xmlPattern, int star);
SQLxD_PRIVATE int checkTheCorrectnessOfPattern(void *pParse, const char*);


SQLxD_API char* stringToLower(const char* str){
        char* temp = (char*) __sqlxd_malloc (strlen(str) + 1);
        strcpy(temp, str);
        int i;
        for(i = 0; temp[i]; i++)
          temp[i] = tolower(temp[i]);
        return temp;
}
SQLxD_API int isNotEmptyString(const char* str){
    int i;
    for (i=0; i<strlen(str); i++)
        if (isgraph(str[i]))
            return i + 1;
    return 0;
}
SQLxD_API char* copyStr(const char* str){
    if (str == 0)
        return (char*)0;
    char* temp = (char*) __sqlxd_malloc( strlen(str) + 1);
    strcpy(temp, str);
    return temp;
}

SQLxD_API char* intToStr(int num){	// max 9 cyfr
	char* str = malloc(10);
	sprintf(str, "%d", num);
	return str;
}

// LIST
SQLxD_API List_string* createList_string(int alloc){
    if (alloc <= 1)
        alloc = 5;
    List_string* list = (List_string*) __sqlxd_malloc(sizeof(List_string));
    list->size = 0;
    list->allocSize = alloc;
    list->tab = (char**) __sqlxd_malloc(sizeof(char*) * alloc);
    return list;
}
SQLxD_API int addToList_string(List_string* list, const char* str){
    if (list == 0 && str == 0)
        return SQLxD_NULL_ARGUMENT;
    if (list->size >= list->allocSize){
        char** tmp = list->tab;
        list->tab = (char**) __sqlxd_malloc(sizeof(char*) * list->allocSize * 2);
        memcpy(list->tab, tmp, sizeof(char*) * list->allocSize);
        list->allocSize *= 2;
        __sqlxd_free(tmp);
    }
    list->tab[list->size] = stringToLower(str);
    ++list->size;

    return 0;
}
SQLxD_API int addUniqueToList_string(List_string* list, const char* str){
  //  print_string(str)
    char* temp = stringToLower(str);
    int i;
    for (i=0; i<list->size; ++i){
        if (strcmp(list->tab[i], temp) == 0){
            __sqlxd_free(temp);
            return 0;
        }
    }
    __sqlxd_free(temp);
    return addToList_string(list, str);
}
SQLxD_API int containsList_string(List_string* list, const char* str){
    if (str == 0)
        return SQLxD_NULL_ARGUMENT;

    char* temp = stringToLower(str);
    int i;
    for (i=0; i<list->size; ++i)
        if (strcmp(list->tab[i], temp) == 0){
            __sqlxd_free(temp);
            return i;
        }
    __sqlxd_free(temp);
    return -248;
}

int list_str_contains (List_string* list, const char* str){
	if (str == 0)
		return -SQLxD_NULL_ARGUMENT;

	int i;
	for (i=0; i<list->size; ++i)
		if (strcasecmp(list->tab[i], str) == 0)
			return 1;
	return 0;
}


SQLxD_API char* getElementList_string(List_string* list, int id){
    if (list == 0 || list->size <= id)
        return 0;
    char* temp = (char*) __sqlxd_malloc(strlen(list->tab[id]) + 1);
    strcpy(temp, list->tab[id]);
    return temp;
}
SQLxD_API void removeList_string(List_string* list){
    deb_in("removeList_string")
    if (list == 0)
        return;
    int i;
    for (i=0; i<list->size; ++i)
        __sqlxd_free(list->tab[i]);
    __sqlxd_free(list->tab);
    __sqlxd_free(list);
    deb_out("removeList_string")
}
SQLxD_API void printList_string(List_string* list){
    int i;
    for (i=0; i<list->size; ++i)
        printf("%s, ", list->tab[i]);
}
SQLxD_API void printlnList_string(List_string* list){
    int i;
    for (i=0; i<list->size; ++i)
        printf("%s\n", list->tab[i]);
}


// REST
SQLxD_API void printList_ptrNode(List_ptrNode* list){
    if (list == 0)
        return;
    int i;
    for (i=0; i<list->nList; ++i)
        printf("%s(%s), ", list->list[i]->name, list->list[i]->tableAlias);
}
SQLxD_API void printNode(Node* node){
    if (node == 0)
        return;

    printf("NODE: name = %s, tableAlias = %s, xmlDocName %s,\tsqlTableName = %s, preceded = ", node->name, node->tableAlias, node->xmlDocName, node->sqlTableName );
    if (node->preceded)
        printf("%s(%s)", node->preceded->name, node->preceded->tableAlias);
    printf(", followed = ");
    if (node->followed)
        printList_ptrNode(node->followed);

    int i;
    for (i=0; i<node->nPattern; ++i){
        printf("\n  %s : ", node->patterns->tab[i]);
        if (node->patternsResolve != 0)
            printList_string(node->patternsResolve[i]);
    }
    printf("\n\n");
}
SQLxD_API void printNodeList(NodeList* nodeList){
    printf("---- NodeList: nList = %d, ------------------------------\n",  nodeList->nList);
    int i;
    for (i=0; i<nodeList->nList; ++i)
        printNode(nodeList->list[i]);

    printf("---- End ofNodeList: -------------------------------------\n");
}

SQLxD_API int addNodeToNodeList(void* pParse, NodeList* nodeList, const char* name, const char* alias){
    if (alias == 0){
        __sqlxd_set_error(pParse, SQLxD_ALIAS_NULL, "Pattern '%s' in 'FROM' statement must have not-null and unique alias", name);
        return SQLxD_ALIAS_NULL;
    }

    if (strlen(name) + 20 > MAX_COLUMN_SIZE){
        __sqlxd_set_error(pParse, SQLxD_PATTERN_TOO_LONG, "Too long XML Pattern name %s",  name);
        return SQLxD_PATTERN_TOO_LONG;
    }
    int i = checkTheCorrectnessOfPattern(pParse, name);
    if ( i != 0)
        return i;
    i = (int) strchr(alias, '.');
    if ( i != 0){
        __sqlxd_set_error(pParse, SQLxD_ALIAS_FAULTY, "Alias '%s' cannot contains dot", alias);
        return SQLxD_ALIAS_FAULTY;
    }

    char* tempAlias  = stringToLower(alias);

    for (i=0; i < nodeList->nList; i++)
        if (strcmp(nodeList->list[i]->tableAlias, tempAlias) == 0){
            __sqlxd_free(tempAlias);
            __sqlxd_set_error(pParse, SQLxD_ALIAS_NOT_UNIQUE, "Duplicate of alias '%s' for node '%s'", alias, name);
            return SQLxD_ALIAS_NOT_UNIQUE;
        }

    if (nodeList->nList >= nodeList->nListAlloc){
        Node** tmp = nodeList->list;
        nodeList->list = __sqlxd_malloc(2 * nodeList->nListAlloc * sizeof(Node*));
        memcpy(nodeList->list, tmp, nodeList->nListAlloc * sizeof(Node*));
        nodeList->nListAlloc *= 2;
    }

    nodeList->list[nodeList->nList] = createNode(name, tempAlias);
    ++nodeList->nList;
    __sqlxd_free(tempAlias);
    return 0;
}
SQLxD_API int addPatternToNodeList(void* pParse, NodeList* nodeList, const char* alias, const char* pattern){
    deb_in("addPatternToNodeList")
    if (alias == 0 || pattern == 0 || nodeList == 0)
        return SQLxD_NULL_ARGUMENT;

    char* tempAlias = stringToLower(alias);
    char* tempPattern = stringToLower(pattern);
    int idx;
    for (idx=0; idx<nodeList->nList; ++idx){
		if (strcmp(nodeList->list[idx]->tableAlias, tempAlias) == 0)
			break;
	}
    if (idx >= nodeList->nList)
    	return SQLxD_ALIAS_NOT_FOUND;

    int rc = checkTheCorrectnessOfPattern(pParse, tempPattern);
//    if (rc != 0)
//        goto endOfAddPatternToNodeList;

    if (rc == 0){
    	addUniqueToList_string(nodeList->list[idx]->patterns, tempPattern);
		nodeList->list[idx]->nPattern = nodeList->list[idx]->patterns->size;
    }

//    int i;
//    for (i=0; i<nodeList->nList; ++i)
//        if (strcmp(nodeList->list[i]->tableAlias, tempAlias) == 0){
//            addUniqueToList_string(nodeList->list[i]->patterns, tempPattern);
//            nodeList->list[i]->nPattern = nodeList->list[i]->patterns->size;
//            goto endOfAddPatternToNodeList;
//        }
 //   __sqlxd_set_error(pParse, SQLxD_ALIAS_NOT_FOUND, "Alias '%s' does not found", alias);

//    endOfAddPatternToNodeList:
    __sqlxd_free(tempAlias);
    __sqlxd_free(tempPattern);
    deb_out("addPatternToNodeList")
    return 0;
}
SQLxD_API int addResolvePatternToNode(Node* node, int idPattern, const char* resolvePattern){   /* nie potrzeba przekazywac bledow do pParse bo one tylko sie pojawia podczas nieprawidlowego uzycia  */
    if (node == 0 && resolvePattern == 0)
        return SQLxD_NULL_ARGUMENT;

    if (idPattern >= node->nPattern)
        return SQLxD_INDEX_OF_BOUND;
    if (node->patternsResolve == 0){
        printf("Error: Need allocate space before add resolve pattern. Call function 'prepareToReadXML(NodeList*)'");
        return SQLxD_MODULE_MISSUSED;
    }

    return addUniqueToList_string(node->patternsResolve[idPattern], resolvePattern);
}
SQLxD_API void addPtrNodeToFollowedList(Node* node, Node* added){
    if  (node == 0 || added == 0)
        return;

    if (node->followed == 0)
        node->followed = createList_ptrNode();

    if (node->followed->nList >= node->followed->nListAlloc) {
        Node** temp = node->followed->list;
        node->followed->list = (Node**) __sqlxd_malloc( sizeof(Node*) * node->followed->nListAlloc * 2);
        memcpy(node->followed->list, temp, sizeof(Node*) * node->followed->nListAlloc);
        node->followed->nListAlloc *= 2;
        __sqlxd_free(temp);
    }
//    printf("addr %d, nList %d, alloc %d\n", node->followed->list, node->followed->nList, node->followed->nListAlloc);
    node->followed->list[node->followed->nList] = added;
    ++(node->followed->nList);
}

SQLxD_PRIVATE Node* createNode(const char* name, const char* alias){
    Node* node = __sqlxd_malloc(sizeof(Node));

    node->name = stringToLower(name);
    node->tableAlias = stringToLower(alias);
    node->xmlDocName = 0;
    node->sqlTableName = 0;

    node->preceded = 0;
    node->followed = 0;
    node->patternsResolve = 0;

    node->nPattern = 1;
    node->patterns = createList_string(5);
    node->dbDocId = 0;
    node->dbSuperNodeId = 0;
    node->dbNodeId = 0;
    node->lastUpdateTime = 0;
    node->newUpdateTime = 0;

    addToList_string(node->patterns, "all_nodes");
    return node;
}
SQLxD_API NodeList* createNodeList(){
    NodeList* nodeList = (NodeList*)__sqlxd_malloc(sizeof(NodeList));
    nodeList->nList = 0;
    nodeList->nListAlloc = 3;
    nodeList->list = (Node**)__sqlxd_malloc(sizeof(Node*) * nodeList->nListAlloc);

    return nodeList;
}
SQLxD_API List_ptrNode* createList_ptrNode(){
    List_ptrNode* list = (List_ptrNode*) __sqlxd_malloc( sizeof(List_ptrNode) );
    list->nList = 0;
    list->nListAlloc = 2;
    list->list = (Node**) __sqlxd_malloc( sizeof(Node*) * list->nListAlloc );
    return list;
}
SQLxD_PRIVATE void createSQLTableName(Node* node){
    char* name = node->name;
    char* tableName = (char*)__sqlxd_malloc(MAX_COLUMN_SIZE);
    strcpy(tableName, XML_TABLE_NODE_PREFIX);

    if (node->preceded != 0)
        sprintf(tableName, "%s_%s_", tableName, getPtrToLastNode(node->preceded->name));

    int i; char c = name[0];
    for (i = 0; c != '\0'; ++i){
        c = name[i];
        if (c == '.'){
            strcat(tableName, "_");
        } else if (c == '?'){
            strcat (tableName, "_q_");
        } else if (c == '*'){
            strcat (tableName, "_s_");
        } else {
            sprintf(tableName, "%s%c", tableName, c);
        }
    }

//    print_string(tableName);
    node->sqlTableName = tableName;
}
SQLxD_PRIVATE void createXmlDocName(Node* node){
    if (node->preceded != 0){
        char* temp = (char*) __sqlxd_malloc( strlen(node->preceded->xmlDocName) + 1);
        strcpy(temp, node->preceded->xmlDocName);
        node->xmlDocName = temp;
        return;
    }

    char* xmlName = (char*) __sqlxd_malloc(MAX_COLUMN_SIZE);
    int dot_index = strchr(node->name,'.') - node->name;
    if (dot_index < 0){
        strcpy(xmlName, node->name);
    } else {
        strncpy(xmlName, node->name, dot_index);
        xmlName[dot_index] = '\0';
    }

    node->xmlDocName = xmlName;
}


SQLxD_API int getResolvePatternListSize(Node* node, int idPattern){
    if (idPattern >= node->nPattern)
        return SQLxD_INDEX_OF_BOUND;
    if (node->patternsResolve == 0){
        printf("Error: Need allocate space before get resolve pattern. Call function 'prepareToReadXML(NodeList*)'");
        return SQLxD_MODULE_MISSUSED;
    }
    return node->patternsResolve[idPattern]->size;
}
SQLxD_API Node* getNodeByAlias(NodeList* nodeList, char* alias){
    if (alias == 0)
        return 0;
    char* tempAlias = stringToLower(alias);
    int i;
    for(i=0; i<nodeList->nList; i++)
        if (strcmp(nodeList->list[i]->tableAlias, tempAlias) == 0){
            __sqlxd_free(tempAlias);
            return nodeList->list[i];
        }
    __sqlxd_free(tempAlias);
    return 0;
}
SQLxD_API char* getSQLTableName(Node* node){
    if (node == 0)
        return 0;
    if (node->sqlTableName == 0)
        createSQLTableName(node);
    char* temp = (char*) __sqlxd_malloc( strlen(node->sqlTableName) + 1);
    strcpy(temp, node->sqlTableName);
    return temp;
}
SQLxD_API char* getXmlDocName(Node* node){
    if (node == 0)
        return 0;
    if (node->xmlDocName == 0)
        createXmlDocName(node);
    char* temp = (char*) __sqlxd_malloc( strlen(node->xmlDocName) + 1);
    strcpy(temp, node->xmlDocName);
    return temp;
}
SQLxD_API char* getFirstNode(const char* str){
    char* temp = (char*) __sqlxd_malloc(strlen(str) + 1);
    int i=0;
    while (str[i] != '\0' && str[i] != '.'){
        temp[i] = str[i];
        ++i;
    }
    temp[i] = '\0';
    return temp;
}
SQLxD_API int getNextNodeIndex(const char* str){
    int i;
    for (i=0; i<strlen(str); ++i)
        if (str[i] == '.')
            return i + 1;
    return -1111111;
}
SQLxD_API const char* getPtrToLastNode(const char* str){
    if (str == 0)
        return 0;
    const char* ptr = &str[strlen(str)];
    while(*ptr != '.' && ptr != str)
        --ptr;
    if (*ptr == '.')
        ++ptr;
    return ptr;
}
SQLxD_API Node* getNodefromFollowedList(Node* node, int ind){
    if (node == 0 || node->followed == 0 || node->followed->nList <= ind)
        return 0;

    return node->followed->list[ind];
}


SQLxD_PRIVATE void remove_Node(Node* node){
    if (node == 0)
        return;
    deb_in("removeNode")
    int i;
    if (node->patternsResolve)
        for (i=0; i<node->nPattern; ++i)
            removeList_string(node->patternsResolve[i]);

    removeList_string(node->patterns);

    __sqlxd_free(node->tableAlias);
    __sqlxd_free(node->sqlTableName);
    __sqlxd_free(node->name);
    __sqlxd_free(node->xmlDocName);
    if ( node->followed )
       __sqlxd_free(node->followed);
    __sqlxd_free(node);
    deb_out("removeNode")
}
SQLxD_API void removeNodeList(NodeList*  nodeList){
    if (nodeList == 0)
        return;
    deb_in("removeNodeList")
    int i;
    for (i=0; i<nodeList->nList; ++i)
        remove_Node(nodeList->list[i]);
    __sqlxd_free(nodeList);
    deb_out("removeNodeList")
}


SQLxD_API void prepareToReadXML(NodeList* nodeList){
    // NOTE allocate space for patterns resolve
    int i,j;
    for (i=0; i<nodeList->nList; ++i){
        Node* node = nodeList->list[i];
        node->patternsResolve = (List_string**) __sqlxd_malloc( sizeof(List_string*) * node->nPattern);
        createSQLTableName(node);
        createXmlDocName(node);
        for (j=0; j<node->nPattern; ++j)
            node->patternsResolve[j] = createList_string(2);
    }
}


SQLxD_PRIVATE const char* getPtrToSecondNode(const char* str){
    char* dotIndex = strchr(str, '.') ;
    return dotIndex == 0 ? str : dotIndex + 1;
}
SQLxD_API int resolvePatterns(void *pParse, NodeList* nodeList){
    Node* node = NULL;
    List_string* allFieldsInThisNode = NULL;
    char* userPattern = NULL;

    int i,j,k;//, dotIndex;
    for (i=0; i<nodeList->nList; ++i){      /// for each node
        node = nodeList->list[i];
        allFieldsInThisNode = node->patternsResolve[0];    /// contains all fields found in XML doc
        for (j=1; j<node->nPattern; ++j){    /// for each pattern in node besides first which contains all fields found in XML doc
            userPattern = node->patterns->tab[j];
            for (k=0; k<allFieldsInThisNode->size; ++k){
                if (comparePatterns(userPattern, getPtrToSecondNode(allFieldsInThisNode->tab[k]) , 0) == 1)
                    addResolvePatternToNode(node, j, allFieldsInThisNode->tab[k]);
            }
            if (node->patternsResolve[j]->size <= 0){
                __sqlxd_set_error(pParse, SQLxD_PATTERN_NOT_FOUND, "Pattern %s.%s does not find in XML", node->name, userPattern);
                return 1;
            }
        }
    }
    return 0;
}
SQLxD_API List_string* getResolvesPatternList(NodeList* nodeList, char* alias, char* pattern){
    if (alias == 0 || pattern == 0 || nodeList == 0)
        return 0;
    Node* node = NULL;
    char* tempAlias = stringToLower(alias);
    char* tempPattern = stringToLower(pattern);
    int i;
    for(i=0; i<nodeList->nList; ++i)
        if (strcmp(nodeList->list[i]->tableAlias, tempAlias) == 0){
            node = nodeList->list[i];
            for (i=0; i<node->nPattern; i++)
                if (strcmp(node->patterns->tab[i], tempPattern) == 0){
                    __sqlxd_free(tempAlias);
                    __sqlxd_free(tempPattern);
                    return node->patternsResolve[i];
                }
        }
    __sqlxd_free(tempAlias);
    __sqlxd_free(tempPattern);
    return 0;
}


//NOTE HELPER FUNCTIONS
SQLxD_PRIVATE int checkTheCorrectnessOfPattern(void *pParse, const char* str){
    if (str == 0)
        return SQLxD_NULL_ARGUMENT;
    char* pattern = stringToLower(str);

    int i, errCode = 0;
    char c;

    if ( pattern[ strlen(pattern) -1 ] == '.'){
        errCode = SQLxD_PATTERN_FAULTY;
        __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: Dot cannot be last element of the pattern", str);
        goto endOfTheCorrectnessOfPattern;
    }
    if (strlen(pattern) == 0){
        errCode = SQLxD_EMPYT_STRING;
        __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: Empty string", str);
        goto endOfTheCorrectnessOfPattern;
    }

    for (i=0; i<strlen(pattern); ++i){
        c = pattern[i];
        //printf("state %d char %c, i = %d\n", state, pattern[i], i);
        switch(c){
            case '.':
                if (i ==0){
                    errCode = SQLxD_PATTERN_FAULTY;
                    __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: Pattern cannot start with dot", str);
                } else {
                    errCode = SQLxD_PATTERN_FAULTY;
                    __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: Sequence of two do in pattern\n", str);
                }
                goto endOfTheCorrectnessOfPattern;

            case '*':  case '?':
                ++i;
                if (pattern[i] == '\0')
                    goto endOfTheCorrectnessOfPattern;
                if (pattern[i] != '.'){
                    errCode = SQLxD_PATTERN_FAULTY;
                    __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: Special chars '?' and '*' cannot occur in node name with other char", str);
                    goto endOfTheCorrectnessOfPattern;
                } else
                    errCode = 0;
                break;

            case '#':
                ++i;
                for (; isAllowedCommonCharInNodeName(pattern[i]); ++i) ;
                if ((pattern[i] == '\0') || (pattern[i] == '.' && pattern[i+1] == '#' && pattern[i+2] == '\0'))
                   goto endOfTheCorrectnessOfPattern;
                errCode = SQLxD_PATTERN_FAULTY;
                __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: End of patten with char '#' must suits to pattern [#([a-z0-9]+\\.#)?]", str);
                goto endOfTheCorrectnessOfPattern;

            default:
                if (c == '\0')
                    goto endOfTheCorrectnessOfPattern;
                if (!isAllowedCommonCharInNodeName(c)){
                    errCode = SQLxD_ILLEGAL_CHAR;
                    __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: Illegal char '%c' in pattern", str, c);
                    goto endOfTheCorrectnessOfPattern;
                }
                for (; isAllowedCommonCharInNodeName(pattern[i]); ++i) ;
                if (pattern[i] != '.' && pattern[i] != '\0'){
                    errCode = SQLxD_PATTERN_FAULTY;
                    __sqlxd_set_error(pParse, errCode, "Wrong pattern '%s', reason: Chars [a-z0-9] cannot be followed by other char than '.' or '\\0'", str);
                    goto endOfTheCorrectnessOfPattern;
                } else
                    errCode = 0;
        }
    }

    endOfTheCorrectnessOfPattern:
    __sqlxd_free(pattern);
    return errCode;
}
SQLxD_API int compareStringsToTheFirstDot(const char* str_1, const char* str_2, int *dotIndex){ /// 1 -  equal, 0 - not equal, return < 0 errorCode
    if (str_1 == 0 || str_2 == 0)
        return SQLxD_NULL_ARGUMENT;
    if (strlen(str_1) == 0 || strlen(str_2) == 0)
        return SQLxD_EMPYT_STRING;

    char* str1 = stringToLower(str_1);
    char* str2 = stringToLower(str_2);
    int i=0;
    while (     str1[i] == str2[i]
                && str1[i] != '\0' && str2[i] != '\0'
                && str2[i] != '.'  && str1[i] != '.')
        ++i;
    if (dotIndex != 0){
        if (str2[i] == '.' || str1[i] == '.')
            *dotIndex = i;
        else
            *dotIndex = -1111111;
    }

    __sqlxd_free(str1);
    __sqlxd_free(str2);
    if (str_1[i] == str_2[i]/* == '\0' */)  /// equal whole strings
        return 1;

    if ((str_1[i] == '\0' && str_2[i] == '.') || (str_2[i] == '\0' && str_1[i] == '.')) /// str1 equal to the dot in str2
        return 1;

    return 0;
}
SQLxD_PRIVATE int comparePatterns(const char* userPattern, const char* xmlPattern, int star){
	// ret < 0 - err, 0 - does not match, 1 -  match

    /// e.g. userPattern a.b.?.*.?.f,  xmlPattern a.b.c.d.e.f
    int u_next = 0, x_next = 0;
    int u_pattern_len = strlen(userPattern);
    int x_pattern_len = strlen(xmlPattern);

    if (x_pattern_len > 2 && xmlPattern[0] == '?')
    	return 0;

    while (u_next < u_pattern_len && u_next >= 0 && x_pattern_len && x_next >= 0){
        //printf("user rest %s, xml rest %s u_next %d  x_next %d \n", userPattern + u_next, xmlPattern + x_next, u_next, x_next);
        if (userPattern[u_next] == '?' || compareStringsToTheFirstDot(userPattern + u_next, xmlPattern + x_next, 0) == 1){
            x_next += getNextNodeIndex(xmlPattern + x_next);
            if (star == 1 && userPattern[u_next] != '?') /// the equation userPattern[u_next] != '?' is faster way of check if compareStringsToTheFirstDot(userPattern + u_next, xmlPattern + x_next, 0) == 1
                if (u_next < u_pattern_len && u_next >= 0 && x_pattern_len && x_next >= 0 && (comparePatterns(userPattern + u_next, xmlPattern + x_next, 1) == 1))
                    return 1;
            if (userPattern[u_next] != '?')
                star = 0;
            u_next += getNextNodeIndex(userPattern + u_next);
        }  else if (userPattern[u_next] == '*'){
            star = 1;
            u_next += getNextNodeIndex(userPattern + u_next);
        }  else if (star) {
            x_next += getNextNodeIndex(xmlPattern + x_next);
        }  else {
            return 0;
        }
    }
//    printf("user rest %s, xml rest %s u_next %d  x_next %d len_user %d len_xml %d\n", userPattern + u_next, xmlPattern + x_next, u_next, x_next, strlen(userPattern), strlen(xmlPattern));
    while (u_next < strlen(userPattern) && u_next >= 0){
        if (userPattern[u_next] != '*')
            return 0;
        else
             u_next += getNextNodeIndex(userPattern + u_next);
    }

    int x_finished = (x_next < strlen(xmlPattern) && x_next > 0) ? 0 : 1;
    int u_finished = (u_next < strlen(userPattern) && u_next > 0) ? 0 : 1;

    if (x_finished && u_finished)
        return 1;

    if (!x_finished && u_finished && star == 1)
        return 1;
    return 0;
}


#endif // __STRUCT_C__
