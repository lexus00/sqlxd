CC=gcc -g
TYPE=$(GOAL)

ifeq ($(OS),Windows_NT)
  RM_CMD=del /q /s *.o *.exe sqlxd-jdbc.dll
  MKDIR_P=mkdir

  EXE=sqlxd.exe
  LIB=sqlxd-jdbc.dll
  TEST=sqlxd_test.exe
  SHELL1=sqlxd_shell.exe

  INCL=-Iinclude
  LIBS=lib/libxml2.lib lib/libxml2_a.lib lib/libxml2_a_dll.lib
  LINKFLAGS=-Wl,--kill-at -shared

  PATH_SEP=\\
else 
  RM_CMD=rm -f *.o sqlxd sqlxd_test sqlxd_shell sqlxd-jdbc.so
  MKDIR_P=mkdir -p

  EXE=sqlxd
  LIB=sqlxd-jdbc.so
  TEST=sqlxd_test
  SHELL1=sqlxd_shell

  INCL=-Iinclude -I/usr/include/libxml2
  LIBS=-lxml2 -pthread -ldl -lm
  LINKFLAGS=-shared

  PATH_SEP=/
endif

JAVA_INCLUDE=-I"C:/Program Files/Java/jdk1.5.0_22/include"

SRC=struct.c json.c parser_xml.c hashmap.c row.c \
	data_loader.c parser_json.c sqlite3.c list.c utils.c
CFLAGS:=$(INCL) -Wall -Wno-unused-function -Wno-format $(CFLAGS) 
LFLAGS=
SQLITE_CFLAGS=-DSQLITE_ENABLE_LOAD_EXTENSION=0  -DSQLITE_ENABLE_UPDATE_DELETE_LIMIT \
   -DSQLITE_ENABLE_COLUMN_METADATA -DSQLITE_CORE -DSQLITE_ENABLE_FTS3 \
   -DSQLITE_ENABLE_FTS3_PARENTHESIS -DSQLITE_ENABLE_RTREE -DSQLITE_ENABLE_STAT2

ifeq ($(TYPE),EXE)
  SRC+=main.c
  OBJ_DIR=obj/exe
  PRODUCT=$(EXE)   
endif
ifeq ($(TYPE),LIB)
  ifneq ($(OS),Windows_NT)
    CFLAGS+=-fPIC
  endif
  SRC+=NativeDB.c
  OBJ_DIR=obj/lib
  LFLAGS=$(LINKFLAGS)
  PRODUCT=obj/lib/$(LIB)
endif
ifeq ($(TYPE),SHELL)
  SRC+=shell.c
  OBJ_DIR=obj/shell
  CFLAGS+=-DSQLXD_SHELL
  PRODUCT=$(SHELL1)
endif
ifeq ($(TYPE),TEST)
  SRC+=StructTest.c ListTest.c MapTest.c MainTest.c SystemTest.c
  OBJ_DIR=obj/test
  CFLAGS+=-DRUN_TEST 	
  PRODUCT=$(TEST)
endif
OBJ=$(addprefix $(OBJ_DIR)/,$(SRC:.c=.o))

.PHONY: all clean exe dll shell test

all:
	$(MAKE) exe
#	@echo options: make [exe dll shell test] 
#	copy database-1000.db database.db 

work:	$(PRODUCT)
	
exe: 
	$(MAKE) GOAL=EXE work
	
dll: 
	$(MAKE) GOAL=LIB work

shell:
	$(MAKE) GOAL=SHELL work
	
test:
	$(MAKE) GOAL=TEST work


 	
$(PRODUCT) : | $(OBJ_DIR) $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(PRODUCT) $(LIBS) $(LFLAGS)

$(OBJ_DIR):
	$(MKDIR_P) $(subst /,$(PATH_SEP),$@)
	

$(OBJ_DIR)/NativeDB.o : jdbc/NativeDB.c
	$(CC) $(INCL) $(JAVA_INCLUDE) -c jdbc/NativeDB.c -o $@

$(OBJ_DIR)/sqlite3.o : src/sqlite3.c	
	$(CC) -c $(CFLAGS) $(SQLITE_CFLAGS) $< -o $@

$(OBJ_DIR)/%Test.o : test/%Test.c
	$(CC) -c $(CFLAGS) $< -o $@
	
$(OBJ_DIR)/%.o : src/%.c
	$(CC) -c $(CFLAGS) $< -o $@
	

clean: 	
	${RM_CMD}
	
