
#ifndef UTILS_H_
#define UTILS_H_

#include "list.h"
#include "hashmap.h"

char* utils_cstr_to_lower (const char*);
int   utils_cstr_is_not_empty (const char*);
char* utils_cstr_concat(const char*, const char*);
char* utils_cstr_copy (const char*);
char* utils_cstr_int_to_cstr (int);
int   utils_cstr_eq_fn (void*, void*);
int   utils_cstr_contains(const char*, char);

#define CSTR_DEL_FN  free
#define CSTR_EQ_FN 	 utils_cstr_eq_fn



void hmap_print(hashmap*);
void list_print(ArrayList*, char*);

#endif /* UTILS_H_ */
