#ifndef __STRUCT_H__
#define __STRUCT_H__

typedef long long int sqlite3_int64;

char* stringToLower(const char* str);
int isNotEmptyString(const char* str);
char* copyStr(const char* str);
char* intToStr(int);

typedef struct List_string{
    int allocSize;
    int size;
    char** tab;
} List_string;

List_string* createList_string(int alloc);
int addToList_string(List_string* list, const char* str);
int addUniqueToList_string(List_string* list, const char* str);
int containsList_string(List_string* list, const char* str);
int list_str_contains (List_string* list, const char* str);

char* getElementList_string(List_string* list, int id);
void removeList_string(List_string* list);
void printList_string(List_string* list);
void printlnList_string(List_string* list);

struct Node;
typedef struct List_ptrNode{
    struct Node** list;
    int nList;
    int nListAlloc;
} List_ptrNode;

typedef struct Node{ /// \\person
    int nPattern;   /// number of patterns to find
    List_string* patterns;           /// e.g. hobby, *
    List_string** patternsResolve;

    char* tableAlias;       /// e.g. hobbies
    char* sqlTableName;     /// e.g. XML_TABLE_NODE_people_person_hobbies
    char* name;             /// e.g. people.person.hobbies
    char* xmlDocName;       /// e.g. People
    sqlite3_int64 dbDocId;          //
    sqlite3_int64 dbSuperNodeId;    // fields in db
    sqlite3_int64 dbNodeId;         //
    int lastUpdateTime;             //
    int newUpdateTime;              //

    struct Node* preceded;         /// e.g. person  (in hobby node) in statement 'select ... from people.person as person natural join person.hobbies as hobby
    List_ptrNode* followed;         /// e.g. hobbies (in person node) in statement 'select ... from people.person as person natural join person.hobbies as hobby
} Node;
typedef struct NodeList{
    Node** list;
    int nList;
    int nListAlloc;
} NodeList;

// NodeList
NodeList* createNodeList();
int addNodeToNodeList(void* pParse, NodeList* nodeList, const char* name, const char* alias);
void printNode(Node* node);
void printNodeList(NodeList* nodeList);
void removeNodeList(NodeList*  nodeList);

// List_ptrNode
void addPtrNodeToFollowedList(Node* node, Node* added);
// private
List_ptrNode* createList_ptrNode();
void printList_ptrNode(List_ptrNode* list);
int getList_ptrNodeSize(Node* node);
Node* getNodefromFollowedList(Node* node, int ind);

// Node
int addPatternToNodeList(void* pParse, NodeList* nodeList, const char* alias, const char* pattern);
int addResolvePatternToNode(Node* node, int idPattern, const char* resolvePattern);
int getResolvePatternListSize(Node* node, int idPattern);

Node* getNodeByAlias(NodeList* nodeList, char* alias);
void prepareToReadXML(NodeList* nodeList);
int resolvePatterns(void *pParse, NodeList* nodeList);
List_string* getResolvesPatternList(NodeList* nodeList, char* alias, char* pattern);
int compareStringsToTheFirstDot(const char* str1, const char* str2, int *dotIndex);
char* getFirstNode(const char* str);
char* getXmlDocName(Node* node);
char* getSQLTableName(Node* node);
const char* getPtrToLastNode(const char* str);
int getNextNodeIndex(const char* str);

// private use only in tests
#ifdef  RUN_TEST

const char* getPtrToSecondNode(const char* str);
int checkTheCorrectnessOfPattern(void *pParse, const char* str);
int comparePatterns(const char* userPattern, const char* xmlPattern, int star);
void createSQLTableName(Node* node);
void createXmlDocName(Node* node);
Node* createNode(const char* name, const char* alias);
void remove_Node(Node* node);

#endif // RUN_TEST

#endif // __STRUCT_C__
