
#ifndef PARSER_H_
#define PARSER_H_

struct sqlite3;
struct NodeList;

int sqlxd_load_data_from_documents(struct NodeList* nodeList, struct sqlite3* db);

#endif /* PARSER_H_ */
