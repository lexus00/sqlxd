#ifndef __MACROS__
#define __MACROS__

/// NOTE PROPERTIES


    #define MAX_COLUMN_SIZE 128
    #ifdef RUN_TEST
        #define SQLxD_API
        #define SQLxD_PRIVATE
    #else
        #define SQLxD_API
        #define SQLxD_PRIVATE static
    #endif // RUN_TEST


    #define ASSERT_FALSE(X)  if ((rc = (X))) return rc;

	#define ASSERT_FALSE_SIMPLE(X) if (X) return 1;

    //printf("Error code: %d msg: %s\n", db->errCode, sqlite3_errmsg(db));

/// NOTE MACROS
    #define print_int(x) printf(#x " = %d\n", x);
    #define print_char(x) printf(#x " = %c\n", x);
    #define print_string(x) printf(#x " = %s\n", x);
    #define print_u32(a) printf(#a " = 0x%08x\n", a);

    #define print_int_white(x,nr) printf("%*.0i" #x " = %d\n", nr, 0, x);
    #define print_char_white(x,nr) printf("%*.0i" #x " = %c\n", nr, 0, x);
    #define print_string_white(x,nr) printf("%*.0i" #x " = -%s-\n", nr, 0, x);
    #define print_u32_white(a, nr) printf("%*.0i" #a " = 0x%08x\n", nr, 0, a);

    #define isAllowedCommonCharInNodeName(x) (((x) >= 'a' && (x) <= 'z') || ((x) >= '0' && (x) <= '9'))
    #define isAllowedCharInNodeName(x) ((x) == '#' || (x) == '?' || (x) == '*' || ((x) >= 'a' && (x) <= 'z') || ((x) >= '0' && (x) <= '9'))



/// NOTE debug & analyze
    #ifdef SIMPLE_ANALYZE_MSG
        #define mprint(s, ...)    printf(s, ##__VA_ARGS__);
    #else
        #define mprint(s, ...)
    #endif // SIMPLE_ANALYZE_MSG

    #ifdef SIMPLE_DEBUG_MODE
        #define deb         printf("line %d\n", __LINE__);
        #define deb_in(s)   printf("line %-6d begin   %s\n", __LINE__, s);
        #define deb_out(s)  printf("line %-6d end     %s\n", __LINE__, s);
    #else
        #define deb
        #define deb_in(s)
        #define deb_out(s)
    #endif

//    #define RUN_SEPARATELY
    #ifdef RUN_SEPARATELY
        #define sqlxd_free free
        #define sqlxd_malloc malloc
    #endif // RUN_SEPARATELY


	#define XML_TYPE 	1
	#define JSON_TYPE	2


/// NOTE ERROR CODES
    #define SQLxD_OK                            0
    #define SQLxD_NULL_ARGUMENT                 201
    #define SQLxD_ALIAS_NOT_FOUND               202
    #define SQLxD_ALIAS_NOT_UNIQUE              203
    #define SQLxD_ALIAS_NULL                    204
    #define SQLxD_ALIAS_FAULTY                  205
    #define SQLxD_DB_ERROR                      206
    #define SQLxD_EMPYT_STRING                  207
    #define SQLxD_FILE_CANNOT_BE_READ           225
    #define SQLxD_FILE_UNKNOWN_TYPE             215
    #define SQLxD_INDEX_OF_BOUND                208
    #define SQLxD_ILLEGAL_CHAR                  209
    #define SQLxD_MODULE_MISSUSED               210
    #define SQLxD_PATTERN_FAULTY                211
    #define SQLxD_PATTERN_TOO_LONG              212
    #define SQLxD_PATTERN_NOT_FOUND             213
    #define SQLxD_USED_REVERSED_CHARS           214
    #define SQLxD_PATTERN_NOT_IMPLICIT          216
    #define SQLxD_TOO_MANY_FIELDS_IN_RESULT     217
    #define SQLxD_UNSUPPORTED_OPERATION         218

	#define SQLxD_FILE_NOT_EXISTS				219
	#define SQLxD_FILE_IO_ERROR					230
	#define SQLxD_FILE_WRONG_FORMAT				231

#endif // __MACROS__
