
#ifndef XML_PARSER_H_
#define XML_PARSER_H_

#include "struct.h"
#include "row.h"

int sqlxd_process_xml_document(const void* blob, int length, Node* node, ROW* row);

int sqlxd_process_json_document(const void* blob, int length, Node* node, ROW* row);

#endif /* XML_PARSER_H_ */
