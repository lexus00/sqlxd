
#ifndef STRUCTS_H_
#define STRUCTS_H_

#include "hashmap.h"

struct sqlite3;
struct Node;

typedef struct ROW {

	struct sqlite3* db;

	hashmap* data;

	int (*on_collect_data)(struct Node* node, void*);

} ROW ;

ROW* row_mk(struct sqlite3* db, int (*fun)(struct Node*, void*));
void row_free(ROW* row);

int row_contains(ROW* row, char* column_name);

int row_add(ROW* row, char* key, char* val);

void row_print(ROW* row);

#endif /* STRUCTS_H_ */
