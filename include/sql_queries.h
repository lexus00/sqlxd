#ifndef SQL_QUERIES_H_INCLUDED
#define SQL_QUERIES_H_INCLUDED

// TABLE NAMES
#define XML_STORE_TABLE "sqlxd_store_table"
#define XML_CREATED_TABLES "sqlxd_created_tables"
#define XML_TABLE_NODE_PREFIX "sqlxd_node_"
#define XML_TABLE_NAME__ORGINAL_DOCUMENTS "sqlxd_original_docs"


// COLUMN NAMES
#define ROW_ID "[?id?]"
#define DOC_ID "[?docId?]"
#define NODE_ID "[?nodeId?]"
#define SUPER_NODE_ID "[?superNodeId?]"

#define DOC_NAME "doc_name"
#define SQL_TBL_NAME "sql_table_name"
#define LAST_UPDATED_DATE "last_update_date"
#define LOADED_DATE "loaded_date"
#define DOC_BLOB "doc"





// Table: 	Created Tables
//#define CREATE_TABLE_ORGINAL_DOCUMENTS "CREATE TABLE %s (id INTEGER PRIMARY KEY, content BLOB);"
#define CT_CREATE_TABLE "CREATE TABLE " XML_CREATED_TABLES " (" \
		SQL_TBL_NAME " TEXT PRIMARY KEY, " DOC_NAME " TEXT, " LAST_UPDATED_DATE " INTEGER);"
#define CT_SELECT_LAST_UPDATE "SELECT " LAST_UPDATED_DATE " FROM " XML_CREATED_TABLES \
	" WHERE " SQL_TBL_NAME " like '%s';"
#define CT_SELECT_TABLE_NAME "SELECT " SQL_TBL_NAME " FROM " XML_CREATED_TABLES \
	" WHERE " DOC_NAME " like '%s';"
#define CT_CREATE_DROP_NODE_TABLES_QUERIES "SELECT 'drop table [' || " SQL_TBL_NAME \
		" || '];' FROM " XML_CREATED_TABLES " WHERE " DOC_NAME " like '%s';"
#define CT_UPDATE_LAST_UPDATE "UPDATE " XML_CREATED_TABLES " set " LAST_UPDATED_DATE \
	" = %d where " SQL_TBL_NAME " like '%s';"
#define CT_INSERT "INSERT INTO " XML_CREATED_TABLES " values('%s','%s', %d);"



// Table: Store Table
#define ST_CREATE_TABLE "CREATE TABLE " XML_STORE_TABLE " (id INTEGER PRIMARY KEY, " DOC_NAME " TEXT, " DOC_BLOB " BLOB, " LOADED_DATE " INTEGER, type INTEGER);"
#define ST_SELECT_DOC_NAME "SELECT DISTINCT " DOC_NAME " FROM " XML_STORE_TABLE ";"
#define ST_SELECT_DOC_NAME_EQ_ID "SELECT " DOC_NAME " FROM " XML_STORE_TABLE " WHERE id = %ld;"
#define ST_SELECT_WITHOUT_DATA "SELECT id, " DOC_NAME ", " LOADED_DATE ", type FROM " XML_STORE_TABLE ";"
#define ST_DELETE_FILE_EQ_ID "DELETE FROM " XML_STORE_TABLE " WHERE id = %ld;"


#define RCT_DELETE_ROW "DELETE FROM %s WHERE " DOC_ID " = %ld;"


#define INSERT_INTO__STORE_TABLE "INSERT INTO " XML_STORE_TABLE \
	" (" DOC_BLOB ", " LOADED_DATE ", " DOC_NAME ", type) VALUES (?, %d, '%s', %d);"
#define SELECT__STORE_TABLE "SELECT " DOC_BLOB ", " LOADED_DATE ", id, type FROM " \
	XML_STORE_TABLE " WHERE " DOC_NAME " like '%s';"


#define PRAGMA_TABLE_INFO "PRAGMA TABLE_INFO ('%s')"

#define SELECT__DOC_NAME__LIKE "SELECT 1 FROM [%s] WHERE " DOC_NAME " = '%s'"
#define DELETE__DOC_NAME__LIKE "DELETE FROM [%s] WHERE " DOC_NAME " like '%s';"

#define CREATE_NODE_TABLE "CREATE TABLE [%s] (" ROW_ID " INTEGER PRIMARY KEY, " DOC_ID " INTEGER NOT NULL, " SUPER_NODE_ID " INTEGER, " NODE_ID " INTEGER NOT NULL);"
#define DROP_TABLE "DROP TABLE [%s]"
#define DROP_TABLE_IF_EXISTS "DROP TABLE IF EXISTS [%s];"
#define DELETE_FROM_TABLE "DELETE FROM [%s]"

#define IS_TABLE_EXIST "SELECT 1 FROM [%s] LIMIT 1;"

#define ADD_COLUMN "ALTER TABLE [%s] ADD [%s] TEXT"

#define INSERT_ROW_PREFIX "INSERT INTO [%s](" DOC_ID ", " NODE_ID ", " SUPER_NODE_ID ""
#define INSERT_ROW_VALUES_PREFIX "VALUES (%I64d, %I64d, %I64d"


#endif // SQL_QUERIES_H_INCLUDED
