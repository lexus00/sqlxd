#ifndef LIST_H_
#define LIST_H_

#define LIST_INITIAL_SIZE 	2 << 6 // use a power of 2 for faster array access
#define LIST_GROWTH_RATE 	2

typedef void* item;

#include <stdint.h>
//#include <stdlib.h>

typedef struct ArrayList {
	item* arr;
	uint32_t size;
	uint32_t capacity;

	void (*del_fn)(item);
	int (*eq_fn)(item, item); // 0 not eq, 1 eq
} ArrayList;


ArrayList* list_mk(void (*del_fn)(item), int (*eq_fn)(item, item));
void list_free(ArrayList*);
void list_clean(ArrayList*);

int __list_add(ArrayList*, item it);
#define list_add(list,it) __list_add(list, (item) it)

int __list_index_of (ArrayList*, item );
#define list_index_of(list,it) __list_index_of (list, (item)it)
#define list_contains(list,it) (__list_index_of (list, (item)it) >= 0)

item list_get(ArrayList*, uint32_t);


#endif /* LIST_H_ */
